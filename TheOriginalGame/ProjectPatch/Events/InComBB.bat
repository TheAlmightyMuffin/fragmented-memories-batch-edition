@echo off
setlocal enabledelayedexpansion
TITLE InCom Official Event
set incomtwo=0
cd.
if exist data\%name%.data set load=1
for /f %%a in (data\%name%.data) do set %%a
if %incomtwo% == 1 (
set bossincomhp=1000000
goto menu1
)
set bossincomhp=12000
:menu1
cls
echo InCom Beta Battle
echo.
echo 1: Start Battle
echo 2: Leave
set /p letter=
if %letter% == 1 goto eventbossincom
if %letter% == 2 goto back
:eventbossincom
cls
echo Corupted InCom(ICTP) appeared!
pause >nul
goto fighteventincom
:fighteventincom
cls
echo %name%'s HP: %hp%
echo InCom's HP: ????
echo.
echo 1: Attack
echo 2: Magic
echo 3: Potions
set /p input=
if %input% == 1 goto attackeventincom
if %input% == 2 goto magiceventincom
if %input% == 3 goto drinkpoteventincom
if %bossfirehp% leq 0 goto killedeventincom
:attackeventincom
cls
set /a bossincomhp=%bossincomhp% - (%power% + %weaponpow%)
if %bossincomhp% leq 0 goto killedeventincom
set /a hp=%hp% - (275)
if %hp% leq 0 goto death
goto fighteventincom
:magiceventincom
cls
set /a bossincomhp=%bossincomhp% - %magic%
if %bossincomhp% leq 0 goto killedeventincom
set /a hp=%hp% - (300)
if %hp% leq 0 goto death
goto fighteventincom
:drinkpoteventincom
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto fighteventincom
)
set /a hp=%hpmax%
set /a pots=%pots% -1
goto fighteventincom
:killedeventincom
cls
echo You defeated Corrupted InCom
echo you got 4,000 EXP
echo and $4,000
set /a exp=%exp% + 4000
set /a money=%money% + 4000
pause >nul
set /a bossincomhp=1000000
set incomtwo=1
goto back
:back
cls
echo Now going back to the main game
ping localhost -n 2 >nul
cls
echo Now going back to the main game.
ping localhost -n 2 >nul
cls
echo Now going back to the main game..
ping localhost -n 2 >nul
TITLE InCom
cls
echo Now going back to the main game...
ping localhost -n 2 >nul
cls
echo Now going back to the main game....
ping localhost -n 2 >nul
cls
goto save
:death
cls
echo You Died!
set /a hp=%hpmax%
pause >nul
goto back
:save
cls
cd .
echo.
(echo hp=%hp%)>> Saves\%name%.SAV
(echo hpmax=%hpmax%)>> Saves\%name%.SAV
(echo exp=%exp%)>> Saves\%name%.SAV
(echo exptill=%exptill%)>> Saves\%name%.SAV
(echo money=%money%)>> Saves\%name%.SAV
(echo lvl=%lvl%)>> Saves\%name%.SAV
(echo power=%power%)>> Saves\%name%.SAV
(echo powergain=%powergain%)>> Saves\%name%.SAV
(echo weaponpow=%weaponpow%)>> Saves\%name%.SAV
(echo weaponprice=%weaponprice%)>> Saves\%name%.SAV
(echo pots=%pots%)>> Saves\%name%.SAV
(echo armupgrade=%armupgrade%)>> Saves\%name%.SAV
(echo room=%room%)>> Saves\%name%.SAV
(echo asword=%asword%)>> Saves\%name%.SAV
(echo incomtwo=%incomtwo%)>> data\%name%.data
call InCom.bat