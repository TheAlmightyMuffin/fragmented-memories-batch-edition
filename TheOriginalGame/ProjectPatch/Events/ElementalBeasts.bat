@echo off
cls
TITLE InCom Official Event
set bossfirehp=5000
set bosswaterhp=5000
set bossgrasshp=5000
:eventmain
cls
echo Choose one
echo.
echo 1) Fire
echo 2) Water
echo 3) Grass
echo 4) Leave
set /p elementchoice=
if %elementchoice% == 1 goto eventbossfire
if %elementchoice% == 2 goto eventbosswater
if %elementchoice% == 3 goto eventbossgrass
:eventbossfire
cls
echo the Flarebeast Boss appeared!
pause >nul
goto fighteventfire
:fighteventfire
cls
echo %name%'s HP: %hp%
echo Flarebeast's HP: %bossfirehp%
echo.
echo 1: Attack
echo 2: Magic
echo 3: Potions
set input=5
set /p input=
if %input% == 1 goto attackeventfire
if %input% == 2 goto magiceventfire
if %input% == 3 goto drinkpoteventfire
if %bossfirehp% leq 0 goto killedeventfire
:attackeventfire
cls
set /a bossfirehp=%bossfirehp% - (%power% + %weaponpow%)
if %bossfirehp% leq 0 goto killedeventfire
set /a hp=%hp% - (30)
if %hp% leq 0 goto death
goto fighteventfire
:magiceventfire
cls
set /a bossfirehp=%bossfirehp% - %magic%
if %element% == Water (
set /a bossfirehp=bossfirehp - (%magic% + 15)
)
if %bossfirehp% leq 0 goto killedeventfire
if %element% == Grass (
set /a hp=%hp% - (45)
)
set /a hp=%hp% - (33)
if %hp% leq 0 goto death
goto fighteventfire
:drinkpoteventfire
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto fighteventfire
)
set /a hp=%hpmax%
set /a pots=%pots% -1
goto fighteventfire
:killedeventfire
cls
echo You defeated Flarebeast
echo you got 1400 EXP
echo and $800
set /a exp=%exp% + 1400
set /a money=%money% + 800
pause >nul
set /a bossfirehp=5000
goto back
:eventbosswater
cls
echo the Aquabeast Boss appeared!
pause >nul
goto fighteventwater
:fighteventwater
cls
echo %name%'s HP: %hp%
echo Aquabeast's HP: %bosswaterhp%
echo.
echo 1: Attack
echo 2: Magic
echo 3: Potions
set input=5
set /p input=
if %input% == 1 goto attackeventwater
if %input% == 2 goto magiceventwater
if %input% == 3 goto drinkpoteventwater
if %bossfirehp% leq 0 goto killedeventwater
:attackeventwater
cls
set /a bosswaterhp=%bosswaterhp% - (%power% + %weaponpow%)
if %bosswaterhp% leq 0 goto killedeventwater
set /a hp=%hp% - (30)
if %hp% leq 0 goto death
goto fighteventwater
:magiceventwater
cls
set /a bosswaterhp=%bosswaterhp% - %magic%
if %bosswaterhp% leq 0 goto killedeventwater
set /a hp=%hp% - (33)
if %hp% leq 0 goto death
goto fighteventwater
:drinkpoteventwater
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto fighteventwater
)
set /a hp=%hpmax%
set /a pots=%pots% -1
goto fighteventwater
:killedeventwater
cls
echo You defeated Aquabeast
echo you got 1400 EXP
echo and $800
set /a exp=%exp% + 1400
set /a money=%money% + 800
pause >nul
set /a bosswaterhp=5000
goto back
:eventbossgrass
cls
echo the Florebeast Boss appeared!
pause >nul
goto fighteventgrass
:fighteventgrass
cls
echo %name%'s HP: %hp%
echo Florebeast's HP: %bossgrasshp%
echo.
echo 1: Attack
echo 2: Magic
echo 3: Potions
set input=5
set /p input=
if %input% == 1 goto attackeventgrass
if %input% == 2 goto magiceventgrass
if %input% == 3 goto drinkpoteventgrass
if %bossfirehp% leq 0 goto killedeventgrass
:attackeventgrass
cls
set /a bosswaterhp=%bosswaterhp% - (%power% + %weaponpow%)
if %bosswaterhp% leq 0 goto killedeventwater
set /a hp=%hp% - (30)
if %hp% leq 0 goto death
goto fighteventwater
:magiceventgrass
cls
set /a bosswaterhp=%bosswaterhp% - %magic%
if %bosswaterhp% leq 0 goto killedeventwater
set /a hp=%hp% - (33)
if %hp% leq 0 goto death
goto fighteventwater
:drinkpoteventgrass
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto fighteventwater
)
set /a hp=%hpmax%
set /a pots=%pots% -1
goto fighteventwater
:killedeventgrass
cls
echo You defeated Aquabeast
echo you got 1400 EXP
echo and $800
set /a exp=%exp% + 1400
set /a money=%money% + 800
pause >nul
set /a bosswaterhp=5000
goto back
:back
cls
echo Now going back to the main game
ping localhost -n 2 >nul
cls
echo Now going back to the main game.
ping localhost -n 2 >nul
cls
echo Now going back to the main game..
ping localhost -n 2 >nul
TITLE InCom
cls
echo Now going back to the main game...
ping localhost -n 2 >nul
cls
echo Now going back to the main game....
ping localhost -n 2 >nul
cls
goto save
:death
cls
echo You Died!
set /a hp=%hpmax%
pause >nul
goto back
:save
cls
cd .
echo.
(echo hp=%hp%)>> Saves\%name%.SAV
(echo hpmax=%hpmax%)>> Saves\%name%.SAV
(echo exp=%exp%)>> Saves\%name%.SAV
(echo exptill=%exptill%)>> Saves\%name%.SAV
(echo money=%money%)>> Saves\%name%.SAV
(echo lvl=%lvl%)>> Saves\%name%.SAV
(echo power=%power%)>> Saves\%name%.SAV
(echo powergain=%powergain%)>> Saves\%name%.SAV
(echo weaponpow=%weaponpow%)>> Saves\%name%.SAV
(echo weaponprice=%weaponprice%)>> Saves\%name%.SAV
(echo pots=%pots%)>> Saves\%name%.SAV
(echo armupgrade=%armupgrade%)>> Saves\%name%.SAV
(echo room=%room%)>> Saves\%name%.SAV
(echo asword=%asword%)>> Saves\%name%.SAV
call InCom.bat