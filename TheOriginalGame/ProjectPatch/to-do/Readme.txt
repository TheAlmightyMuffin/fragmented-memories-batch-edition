Thank you for getting InCom
Please read the Q & A if you have any questions

-Q & A

Q: Where did the name come from?
A: INtellegentCOMputer

Q: Is this free?
A: Yes

Q: Will it always be free?
A: Yes

Q: What's the data folder for?
A: Different things, like the InCom Battle event

Q: 'edit' is not recognized as an internal or external command, operable program or batch file.
A: You are running a x64 based Windows, I am NOT working on a fix to this. If you want to edit the game
on a x64 based Windows then open it with your favourite text editor(such as Notepad).

Q: When will this be finished?
A: TBA 

-Release Dates

Beta 1.2: Sep. 26, 2012
Beta 1.3: Oct. 17, 2013
Beta 1.4: TBA
Initial Release: TBA
InCom2: TBA

-FOLDER STRUCTURE

data: Different things

Events: Put event .bats here and go to the event menu in Options

past: Play older versions here

Saves: Where your saves go
  storymode: for story mode
  freemode: for freemode

InCom.bat: Main Game

Quiz.bat: Mini-game to unlock a new special mission

to-do: Random Folder
  Readme.txt: What your reading right now

-Story

InCom takes place in Middle Future as you play as a boy
and a mobile intelligent computer called ICTP as you go on a journey to Forward Mountain,
but gets interrupted by someone with a cruel heart who plans to destroy the planet.

-Upcoming Features

Started as of Saturday, September 8, 2012
(9/25/12: wow, beta 1.2 prereleases went fast, already to pre3)

9/8/12: BETA 1.2 UPDATE: Freemode Demo (freemode will be with Beta 1.3)
9/8/12: BETA 1.2 UPDATE: 3 Endings
9/8/12: BETA 1.2 UPDATE: Extended Story
9/8/12: BETA 1.2 UPDATE: New Battle System (only for new enemies until 1.3)
9/8/12: BETA 1.2 UPDATE: More Enemies(101error, skeleton, yes man, dragan, metal golem, to-be-named final boss(neutral end))
9/8/12: BETA 1.2 UPDATE: The evil lord is now nameable
9/8/12: BETA 1.2 UPDATE: Rewritten cut scene after Iron Golem
9/9/12: BETA 1.2 UPDATE: Project Time Capsule is accessible through the game
9/10/12: BETA 1.2 UPDATE: New Beginning cut scene
9/10/12: Beta 1.1.01 Bugfix
9/22/12: BETA 1.2 UPDATE: Jewels
9/25/12: BETA 1.2 UPDATE: In Game Editor(accessed by typing 9898 in Central Kingdom(edit: now in Extras) once you beat the game. (good ending))
9/25/12: BETA 1.2 UPDATE: Release Date: Friday, September 26, 2012
10/6/12: BeTA 1.3 UPDATE: Special Missions
10/10/12: BeTA 1.3 UPDATE: A new, fancy, loading screen
BeTA 1.3 UPDATE: Freemode (we all saw that coming)
10/10/12: BeTA 1.3 UPDATE: New Battle System COMPLETE (obsolete for older enemies)
10/12/12: BeTA 1.3 UPDATE: Help Topics
10/12/12: BeTA 1.3 UPDATE: 3rd end has became 2nd end and new 3rd end
06/21/13: BeTA 1.3 UPDATE: More story and actually more process on the game, yay
10/8/13: Consideration for continuation (may start today)
10/9/13: Continuation
10/9/13: BeTA 1.3 Pre-release 1: Release Date: Friday, October 11, 2013(Apparently it was almost complete when I stopped)
10/9/13: BeTA 1.3 UPDATE: Release Date: October 17, 2013(Apparently it was almost complete when I stopped)
10/22/13: BeTA 1.4 UPDATE: Fixed typo with 2nd (said 2st)
11/25/13: BeTA 1.4 UPDATE: Save name is now separate from in-game name


-Hints, Tricks, and Glitches (spoilers(maybe))

  -Trick: In Game Editor: Type 9898 in Central Kingdom and you will be able to mod the game once you beat the game. (good ending) (doesn't work in Beta 1.2pre2 and back)
  -Trick: New Battle System: Type 087658 in Central Kingdom(doesn't work on Beta 1.0)
  -Glitch: health50: On the new battle system test, select inventory and select "2)  0". This will mess with your stats, set things like health and power to 1, and changes your element to health50 
  -Glitch: Keys Glitch: Load a save file then at any time go to the Temple of Light. The game will crash. This is caused by the amount of keys not being saved.(Fixed in 1.1.01 Bugfix) WARNING: When updating to Beta 1.1.01 Bugfix or higher, add this to your save file(without the quotes) 'keys=0'. Yes, you do have to fight the Key Guards again.
  -Glitch: Freemode Demo (Beta 1.2 pre1): Save: Try to save two times and the game will crash

- Notes
  -The screen will not clear on WinE
  
- Fun Facts
  -Darkwind used to be called Alpha, and even before he was called just 'Evil'
  -Patch used to do TWELVE TRILLION HP DAMAGE when attacked normally, but the number... was shorten due to bugs

- Jewel Locations

	- Defeat Iron Golem
	- Defeat Steel Golem
	- Defeat Titanium Golem

-Official Names(spoilers(maybe))

  - You: Beta
  - Evil Lord: Darkwind
  - Your Friend: Hard
  - Unstoppable Evil Creature: (none yet)
  - Girl who helped you: Alpha
  - Guy with A lot of Teleporter Keys: Key
  - Boy who ended the Chaos War: Isaac
  - Patch #1: Platinum
  - Patch #2: Server
  - Patch #3: Begin
  - Patch #4: Easy
  - Patch #5: Clause
  - Patch #6: Wind
  - Patch #7: Lucky
  - Patch #8: Memory
  - Patch #11: Gamma
  - Patch #12: Omega
