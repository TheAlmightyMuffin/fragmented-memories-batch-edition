@echo off
setlocal enabledelayedexpansion
TITLE InCom2: Project Patch 12
color 0A
goto loading
:loading
cls
echo Loading, please wait.
echo _
ping localhost -n 2 >nul
cls
echo Loading, please wait.
echo.
echo :
ping localhost -n 2 >nul
cls
echo Loading, please wait.
echo.
echo _
ping localhost -n 2 >nul
cls
echo Loading, please wait.
echo.           
echo  :
ping localhost -n 2 >nul
cls
echo Loading, please wait.
echo _
echo.
ping localhost -n 2 >nul
:menu
cls
echo -------------------------------------
echo Project Patch 12
echo -----ALPHA 0.1.3------------------
echo.
echo.
echo 1) New Game
echo 2) Load Game
set /p letter=
if %letter% == 1 goto new
if %letter% == 2 goto load
if %letter% == call goto callfunction
goto menu
:load
cls
set /p name= Load Name: 
if not exist Saves\storymode\!name!.SAV goto noload
if exist Saves\storymode\!name!.SAV set load=1
for /f %%a in (Saves\storymode\!name!.SAV) do set %%a
goto home
:new
cls
echo I N C O M   2 :   P R O J E C T   P A T C H   1 2
pause >nul
cls
echo It is recommended you play InCom first.
echo.
echo 	Following the events of InCom,
echo a strange boy was walking down Main Street
echo while the paperboy(who updated phones and tablets while 
echo walking down Main Street) was telling people who didn't
echo have an electronic device on them the news.
echo 	He encountered the boy.
echo 	"Hey, have you heard? The big robot that
echo destroyed Central Castle was defeated.
echo Hey, You don't look like you're from 
echo these parts. I would be cautious, there has been
echo a recent uproar in criminals. Where are you from?"
echo 	He said nothing.
echo 	"Well, I have to go."
pause
cls
:newstats
cls
::Temp
set name=???
::Main
set hp=13
set hpmax=13
set exp=0
set exptill=15
set money=5
set lvl=1
set pots=5
set power=4
set powergain=2
set magicgain=2
set weapon=Wooden Sword
set weaponpow=1
set weaponprice=30
set magic=5
set armor=0
set armupgrade=0
set room=0
set keys=0
set asword=0
set jewels=0
set debug=0
set pot50s=0
set final=0
set smission1=0
set direction=east
::Party Members
set alpha=0
set alphahp=25
set ictp=1
set ictphp=10
::Magic
set spellfire1=0
set spellfire2=0
set spellfire3=0
set spellfire4=0
set spellfire5=0
set spellwater1=0
set spellwater2=0
set spellwater3=0
set spellwater4=0
set spellwater5=0
set spellwaterblast1=0
set spellwaterblast2=0
set spellwaterblast3=0
set spellleafcutter1=0
set spellleafcutter2=0
set spellleafcutter3=0
set spellleafcutter4=0
set spellult1=0
set spellult2=0
set spellult3=0
set spellcometcrash=0
echo Well, that's it for now. However,
echo you can try the battling if you'd like
echo Chose an enemy
echo 1) Ghost
echo 2) Book
echo 3) InCom Battle1
echo 4) See more story
set /p input=
if %input% == 1 goto ghost
if %input% == 2 goto book
if %input% == 3 goto incom1
if %input% == 4 goto story1
:ghost
set enemyname=Ghost
set num=gh
set enghhp=10
set enghpow=3
set enghmagic=3
cls
echo A Ghost appeared
pause >nul
goto fight
:killedgh
goto newstats

:book
set enemyname=Book
set num=bk
set enbkhp=25
set enbkpow=7
set enbkmagic=0
cls
echo A Haunted Book appeared
pause >nul
goto fight
:killedbk
goto newstats

:incom1
set enemyname=InCom
set num=ic
set enichp=50
set enicpow=5
set enicmagic=7
cls
echo InCom suddenly attacked
pause >nul
goto fight
:killedic
goto newstats

::actual story now

:story1
cls
echo 	This boy spent hours, days, weeks wandering
echo with nowhere to go. Soon, he found a computer, so he decided to
echo turn it on.
echo 	"BOOTING INCOM OS...BOOTING INCOM UI...BOOTING INCOM INTERFACE"
echo 	"Huh... Where am I? Who are you?" the strange computer said.
echo The boy shrugged. He looked around. "Would you like me to tell
echo about myself?" The boy nodded. "I am a very intellegent computer called InCom, formerly ICTP.
echo I was made in Platinum Labs. Soon, an evil robot called Patch took me away from him.
echo I was helpless. Soon, a boy found me in a box. We became friends."
echo Then, The boy wispered to himself, in a REALLY quiet 
echo voice, "Friends?"
echo "The boy's name was...
pause >nul
cls
echo NAME ENTRY
echo A cyborg who is Patch 10(official name: Beta)
set /p name=
cls
echo 	"His name was !name!. As it turned out, he
echo was part robot. Not just any robot, but Patch 10.
echo He wasn't evil like his creator, Patch. I need to find him. He is the
echo only way to stop Patch. He knows the song. However, not only he has to
echo sing it. Two other people has to sing it with him. It is possible YOU
echo are one of them. I don't know for sure. Well, I better be going."
echo 	InCom left.
pause
cls
echo 	Here we see !name! walking as well
echo 	"Where are they? Where am I even? Oh, the kingdom
echo is over there."
pause
echo WHAT ELEMENT ARE YOU?
echo.
echo 1: Basic
echo 2: Fire
echo 3: Water
echo 4: Grass
set /p input=
if %input% == 1 (
set element=Basic
goto home
)
if %input% == 2 (
set element=Fire
goto home
)
if %input% == 3 (
set element=Water
goto home
)
if %input% == 4 (
set element=Grass
goto home
)
set element=Basic
goto home


:home
set gamemode=1
if "!room!" == "a" goto draganvillage
if %exp% geq %exptill% goto levelup
cls
echo !name!
echo Level %lvl%
echo.
echo Central Kingdom
echo.
echo.
echo What do you want to do?
echo.
echo 1: Outside the kingdom
echo 2: Shop
echo 3: Hospital
echo 4: Stats
echo 5: SaveStation
echo 6: Options
echo 7: Extras
echo 0: Main Menu
set /p input=
if %input% == 1 goto direction
if %input% == 2 goto shop
if %input% == 3 goto healer
if %input% == 4 goto stats
if %input% == 5 goto save
if %input% == 6 goto ingameoption
if %input% == 7 goto home2
if %input% == 0 goto menu
:direction
cls
echo Which way
echo 1) North
echo 2) East
echo 3) West
echo 0) Go back
set /p input=
if "!input!" == "1" goto northkingdom0
if "!input!" == "2" goto eastkingdom0
if "!input!" == "3" goto westkingdom0
if "!input!" == "0" goto home
:ingameoption
cls
echo 1) Info
echo 2) Events
echo 3) Exit
echo 4) Back
set /p letter=
if %letter% == 1 goto info2
if %letter% == 2 goto event4
if %letter% == 3 exit
if %letter% == 4 goto home
:info2
cls
echo In Progress
pause >nul
goto ingameoption
:home2
cls
echo Yes?
echo.
echo 1: PROJECT TIME CAPSULE
echo 2: Editor
echo 3: Special Missions(does not work)
echo 4: Help Topics
echo 0: Back
set /p input=
if "!input!" == "1" goto ptc
if "!input!" == "2" goto edit
if "!input!" == "3" goto home
if "!input!" == "4" goto help
if "!input!" == "0" goto home
goto home
:help
cls
echo Help Topics
echo.
echo 1) How to play
echo 2) Dragans
echo 3) Chaos War
echo 4) Patch
echo 5) What's new
echo 0) Back
set /p input=
if "!input!" == "1" goto helphow
if "!input!" == "2" goto helpdragan
if "!input!" == "3" goto helpchaos
if "!input!" == "4" goto helppatch
if "!input!" == "5" goto helpnew
if "!input!" == "0" goto home2
:helphow
cls
echo In Progress
pause
goto home2
:helpdragan
cls
echo In Progress
pause
goto home2
:helpchaos
cls
echo In Progress
pause
goto home2
:helppatch
cls
echo 	Patch were a series of robots created by an
echo unknown source
echo.
echo 	They include:
echo Patch 1: Platinum
echo Patch 2: Server
echo Patch 3: Begin
echo Patch 4: Wind
echo Patch 5: Mother
echo Patch 6: Clause
echo Patch 7: Lucky
echo Patch 8: Memory
echo Patch 9: !fgname!
echo Patch 10: !name!
echo Patch 11: Gamma
echo Patch 12: Omega?
pause
goto help
:helpnew
cls
echo In Progress
pause
goto home2
:killedtpg3
cls
echo You defeated the TPG
echo you got 40 EXP
set /a exp=%exp% + 40
pause >nul
goto home
:password1
cls
echo THE PASSWORD IS:
echo LikenOotHer12349
pause
goto keyguard1
:shop
cls
echo Hello, %name%, Welcome to my shop
echo.
echo.
echo Money: %money%
echo 1: Weapons
echo 2: Potions
echo 3: EXP Stuff
echo 4: Mechanical Arm $800
echo 0: Exit
::echo 5: More
set /p option=
if %option% == 1 goto weapons
if %option% == 2 goto potions
if %option% == 3 goto expshop
if %option% == 4 goto mecharmshop
if %option% == 0 goto home
::if %option% == 5 goto moreshop
:potions
cls
echo 1: FullHP $20
echo 2: MaxHpUp $80
echo 3: Super-MaxHpUp $500
echo 0: Back
set /p letter=
if %letter% == 1 goto buypot
if %letter% == 2 goto maxhp
if %letter% == 3 goto superhp
if %letter% == 0 goto shop
:maxhp
cls
if %money% lss 80 goto not
echo Your Max HP went up!
pause >nul
set /a money=%money% - 80
set /a hpmax=%hpmax% + 2
goto potions
:superhp
cls
if %money% lss 500 goto not
echo Your Max HP went up!
pause >nul
set /a money=%money% - 500
set /a hpmax=%hpmax% + 50
goto potions
:weapons
cls
echo 1: Sword $50
echo NOT AVAILABLE, SORRY: Bow $100
echo 3: WeaponUpgrade $%weaponprice%
echo NOT AVAILABLE, SORRY: Armor $100
echo NOT AVAILABLE, SORRY: Shield $50
echo NOT AVAILABLE, SORRY: Battle Suit $10000
echo 7: Back
set /p letter=
if %letter% == 1 goto buysword1
::if %letter% == 2 goto buybow1
if %letter% == 3 goto upgradeweapon
::if %letter% == 4 goto buyarmor
if %letter% == 7 goto shop
:upgradeweapon
if %money% lss %weaponprice% goto not
if %asword% == 1 goto notup
if %weaponpow% leq 0 gotonotup
if %weaponpow% geq 140 goto notup
set /a money=%money% - %weaponprice%
set /a weaponpow=%weaponpow% + 4
set /a weaponprice=%weaponprice% + 17
cls
echo Successfuly upgraded...
pause >nul
goto shop
:buysword1
if %money% lss 50 goto not
if %asword% == 1 goto notbuy
set /a money=%money% - 50
set /a weaponpow=50
set /a weapon=Sword
cls
echo Successfuly bought
pause >nul
goto shop
:buybow1
if %money% lss 50 goto not
if %asword% == 1 goto notbuy
set /a money=%money% - 100
set /a weaponpow=45
set /a weapon=Bow
cls
echo Successfuly bought
pause >nul
goto shop
:buyarmor
if %money% lss 100 goto not
set /a money=%money% - 100
set /a armor=50
cls
echo Successfuly bought
pause >nul
goto shop
:notup
echo I can't upgrade that!
pause >nul
goto shop
:notbuy
echo Use your weapon instead!
pause >nul
goto shop
:buypot
if %money% lss 20 goto not
set /a pots=%pots% + 1
set item!itemsmax!=Potion
set /a money=%money% - 20
goto shop
:expshop
cls
echo 1: EXP Orbs $5
echo 2: Instant Level Up $50
echo 3: Back
set /p letter=
if %letter% == 1 goto orbshop
if %letter% == 2 goto levelshop
if %letter% == 3 goto shop
:orbshop
if %money% lss 5 goto not
set /a exp=%exp% + 2
set /a money=%money% - 5
goto shop
:mecharmshop
if %armupgrade% geq 1 goto noarm
if %money% lss 800 goto not
set /a power=%power% + 320
set /a money=%money% - 800
set armupgrade=1
goto shop
:levelshop
if %money% lss 50 goto not
if %lvl% geq 100 goto lvlupno
echo You Leveled Up
pause >nul
set /a money=%money% - 50
set /a lvl=%lvl% + 1
if %power% geq 120(set /a power=120)
set /a power=%power% + %powergain%
set /a hpmax=%hpmax% + 4
set /a magic=%magic% + 4
goto shop
:not
cls
echo You don't have enough money!
pause >nul
goto shop
:noarm
cls
echo You already have a Mechanical Arm!
pause >nul
goto shop
:healer
cls
echo Welcome, %name%
echo Do you want to get healed?
echo.
echo 1) Yes
echo 2) No
set input=2
set /p input=...
if %input% equ 1 goto healer2
goto home
:healer2
set hp=%hpmax%
goto home
:northkingdom0
cls
if "!direction!" == "east" goto home
if "!direction!" == "west" goto home
:westkingdom0
cls
if "!direction!" == "east" goto home
if "!room!" == "10" goto westkingdom
if "!room!" == "11" goto westkingdom1
if "!room!" == "12" goto westkingdom2
if "!room!" == "13" goto westkingdom3
if "!room!" == "14" goto westkingdom4
if "!room!" == "15" goto westkingdom5
if "!room!" == "16" goto westkingdom6

:westkingdom
cls
set enemyname=Ghost
set enghosthp=10
set num=ghost
set enghostpow=3
set enghostmagic=3
set en1hp=10
cls
echo The Ghost appeared
pause >nul
goto fight
:killedghost
cls
echo You defeated the ghost
echo you got 8 EXP
echo and $3
set /a exp=%exp% + 8
set /a money=%money% + 3
pause >nul
cls
echo 	"No, stop, please! I'm not bad!" the ghost shouted.
echo "Huh, it's you," !name! said. "Oh no, its him,"
echo the ghost said. Then a person with a strange rifle
echo showed up. "Hey, it's you. I see you got friends with you,"
echo Then, he shot the ghost. However, it was a dart. She fell on
echo the ground. "Well, I call myself a Ghost Breaker, not to 
echo be confused with Ghostbusters."
echo 	!name! decided to attack!
pause >nul
cls
set enemyname=Ghost Breaker
set enghbkhp=74
set num=ghbk
set enghbkpow=24
set enghbkmagic=50
cls
echo The Ghost Breaker attacked
pause >nul
goto fight
:killedghbk
cls
echo You defeated the Ghost Breaker
echo you got 2 EXP
echo and $10
set /a exp=%exp% + 2
set /a money=%money% + 10
pause >nul
cls
echo 	"Okay... I'll give the ghost to you. I've injected
echo a special type of fever that only affects ghosts." He sighed,
echo then left.
echo.
echo You took the ghost into your care.


:eastkingdom0
cls
if "!room!" == "0" goto eastkingdom
if "!room!" == "1" goto eastkingdom1
if "!room!" == "2" goto eastkingdom2
if "!room!" == "3" goto eastkingdom3
if "!room!" == "4" goto eastkingdom4
if "!room!" == "5" goto eastkingdom5
if "!room!" == "6" goto eastkingdom6
if "!room!" == "7" goto eastkingdom7
if "!room!" == "8" goto eastkingdom8
if "!room!" == "9" goto eastkingdom9

:eastkingdom
cls
echo 	"Well, I went west last time. I'm going east this time."
pause
:eastkingdom1
set room=1
cls
echo What to do?
echo 1) Go forward
echo 2) Go back
echo 3) Go home
set /p input=
if "!input!" == "1" goto enemydog
if "!input!" == "2" goto home
if "!input!" == "3" goto home
:enemydog
set enemyname=Stray Dog
set num=dg
set endghp=7
set endgpow=5
set endgmagic=5
cls
echo A Stray Dog appeared
pause >nul
goto fight
:killeddg
cls
echo You defeated the dog
echo you got 13 EXP
echo and 10$
set /a exp=%exp% + 13
set /a money=%money% + 10
pause >nul
set room=2
if "!exp!" geq "!exptill!" goto levelup
:eastkingdom2
set room=2
cls
echo What to do?
echo 1) Go forward
echo 2) Go back
echo 3) Go home
set /p input=
if "!input!" == "1" goto eastkingdom3
if "!input!" == "2" goto eastkingdom1
if "!input!" == "3" goto home
:eastkingdom3
set room=3
cls
echo What to do?
echo 1) Go forward
echo 2) Go back
echo 3) Go home
set /p input=
if "!input!" == "1" goto enemyskeleton
if "!input!" == "2" goto eastkingdom2
if "!input!" == "3" goto home
:enemyskeleton
cls
echo The Skeleton from the west path appeared!
pause >nul
set enemyname=Skeleton
set num=sk
set enskpow=7
set enskmagic=9
set enskhp=23
goto fight
:killedsk
cls
echo You defeated the skeleton
echo You got 14 exp
echo and $11
pause >nul
set /a exp=!exp! + 140
set /a money=!money! + 100
set room=4
if "!exp!" geq "!exptill!" goto levelup
:eastkingdom4
set room=4
cls
echo What to do?
echo 1) Go forward
echo 2) Go back
echo 3) Go home
set /p input=
if "!input!" == "1" goto eastkingdom5
if "!input!" == "2" goto eastkingdom3
if "!input!" == "3" goto home



::##BATTLE######################################

:fight
cls
echo !enemyname!'s HP: !en%num%hp!
if "!alpha!" == "1" (
	echo !fgname!'s HP: !alphahp!
)
::if "!enid!" == "000" (
::	echo !enemyname!'s HP: ????
::)
::if "!enid!" neq "000"
echo.
echo 1: Attack
echo 2: Magic
echo 3: Items
echo.
echo !name!'s HP: !hp!
set /p input=
if "!input!" == "1" (
	if "!alpha!" == "1" goto fight2
	goto attack
)
if %input% == 2 goto magiclist
if %input% == 3 goto inventory
:fight2
set alphaatk=0
set alphapot=0
set alphamgc=0
cls
echo %name%'s HP: %hp%
echo !fgname!'s HP: !alphahp!
echo.
echo 1: Attack
echo 2: Magic
echo.
echo !enemyname!'s HP: !en%num%hp!
set /p input=
if %input% == 1 (
	set alphaatk=1
	goto attack
	)
if %input% == 2 (
	set alphamgc=1
	goto magic
	)
:attack
cls
set /a en%num%hp=!en%num%hp! - (%power% + %weaponpow%)
if "!alphaatk!" == "1" (
	set /a en%num%hp=!en%num%hp! - %alphapow%
)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%pow!)
if %hp% leq 0 goto deathchoice
goto fight
:magiclist
cls
echo Magic
echo.
echo 1) Basic Offense
echo 2) Fire I
echo 3) Water I
echo 4) Leaf Razor I
echo 5) Waterblast I
echo 6) Thunder I
echo 7) Ultimate I
echo 8) Comet Crash
set /p input=
if "!inupt!" == "1" goto magicuse
if "!input!" == "2" goto usefire1
if "!input!" == "3" goto useult1
goto fight
:nomagic
cls
echo You do not have that spell...
pause
goto magiclist
:usefire1
if not "!spellfire1!" == "1" goto nomagic
cls
set /a en%num%hp=!en%num%hp! - (%magic% - 15)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:usewater1
if not "!spellwater1!" == "1" goto nomagic
cls
set /a en%num%hp=!en%num%hp! - (%magic% - 15)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:useult1
if not "!spellult1!" == "1" goto nomagic
cls
set /a en%num%hp=!en%num%hp! - (%magic% - 75)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:drinkpot
cls
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto inventory
)
set /a hp=%hp% +20
set /a pots=%pots% -1
goto fight
:drinkpot2
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto fight
)
set /a hp=%hpmax%
set /a pots=%pots% - 1
goto fight
:killed
call :killed%num%

:inventory
cls
echo Items
echo.
echo 1) Small-Potions: %pots%
echo 2) Medium-Potions: !pot50s!
echo 3) High-Potions: !pot100s!
echo 4) Mega-Potions: !pot200s!
echo 5) Max-Potions: !potmaxs!
echo 0) Back
set /p input=
if "!input!" == "1" goto drinkpot2
if "!input!" == "0" goto fight
goto fight

:deathchoice
cls
echo You were defeated...
pause >nul
goto home

::##OTHER###############################################

:levelup
if %lvl% geq 100 goto lvlupno
cls
echo You Leveled Up
pause >nul
set /a lvl=%lvl% + 1
set /a exp=%exp% - %exptill%
set /a exptill=%exptill% + %exptill% * 66 / 100
set /a power=%power% + %powergain%
set /a hpmax=%hpmax% + 4
set /a hp=%hp% + 4
set /a magic=%magic% + %magicgain%
set /a room=!room! + 1
goto spell
:spell
cls
echo You learned:
if "!lvl!" == "4" (
	if "!element!" == "Basic" (
		echo It was Fire I
		echo and Water I
		set spellfire1=1
		set spellwater1=1
	)
	if "!element!" == "Water" (
		echo It was Water II
		set spellwater2=1
	)
)
if "!lvl!" == "7" (
	if "!element!" == "Basic" (
		echo It was Leaf Cutter I
		set spellleafcutter1=1
	)
	if "!element!" == "Grass" (
		echo It was Leaf Cutter II
		set spellleafcutter2=1
	)
)
pause
call :!direction!kingdom!room!
:lvlupno
goto home

:save
cls
echo Saving...
(echo hp=%hp%)>> Saves\storymode\%name%.SAV
(echo hpmax=%hpmax%)>> Saves\storymode\%name%.SAV
(echo exp=%exp%)>> Saves\storymode\%name%.SAV
(echo exptill=%exptill%)>> Saves\storymode\%name%.SAV
(echo money=%money%)>> Saves\storymode\%name%.SAV
(echo lvl=%lvl%)>> Saves\storymode\%name%.SAV
(echo power=%power%)>> Saves\storymode\%name%.SAV
(echo powergain=%powergain%)>> Saves\storymode\%name%.SAV
(echo weaponpow=%weaponpow%)>> Saves\storymode\%name%.SAV
(echo weaponprice=%weaponprice%)>> Saves\storymode\%name%.SAV
(echo pots=%pots%)>> Saves\storymode\%name%.SAV
(echo armupgrade=%armupgrade%)>> Saves\storymode\%name%.SAV
(echo room=%room%)>> Saves\storymode\%name%.SAV
(echo magic=%magic%)>> Saves\storymode\%name%.SAV
(echo asword=%asword%)>> Saves\storymode\%name%.SAV
(echo keys=%keys%)>> Saves\storymode\%name%.SAV
(echo element=%element%)>> Saves\storymode\%name%.SAV
(echo jewels=%jewels%)>> Saves\storymode\%name%.SAV
(echo pot50s=%pot50s%)>> Saves\storymode\%name%.SAV
(echo elname=%elname%)>> Saves\storymode\%name%.SAV
(echo ecname=%ecname%)>> Saves\storymode\%name%.SAV
(echo frname=%frname%)>> Saves\storymode\%name%.SAV
(echo fgname=%fgname%)>> Saves\storymode\%name%.SAV
(echo kgname=%kgname%)>> Saves\storymode\%name%.SAV
(echo awname=%awname%)>> Saves\storymode\%name%.SAV
(echo dragan=%dragan%)>> Saves\storymode\%name%.SAV
(echo talk=%talk%)>> Saves\storymode\%name%.SAV
(echo talk2=%talk2%)>> Saves\storymode\%name%.SAV
(echo talk3=%talk3%)>> Saves\storymode\%name%.SAV
(echo talk4=%talk4%)>> Saves\storymode\%name%.SAV
(echo talk5=%talk5%)>> Saves\storymode\%name%.SAV
(echo talk6=%talk6%)>> Saves\storymode\%name%.SAV
(echo talk7=%talk7%)>> Saves\storymode\%name%.SAV
(echo talk8=%talk8%)>> Saves\storymode\%name%.SAV
(echo final=%final%)>> Saves\storymode\%name%.SAV
(echo smission1=%smission1%)>> Saves\storymode\%name%.SAV
(echo cantgetjewel=%cantgetjewel%)>> Saves\storymode\%name%.SAV
(echo finalmessage=%finalmessage%)>> Saves\storymode\%name%.SAV
(echo ictp=%ictp%)>> Saves\storymode\%name%.SAV
(echo alphahp=%alphahp%)>> Saves\storymode\%name%.SAV
(echo alpha=%alpha%)>> Saves\storymode\%name%.SAV
(echo magicgain=%magicgain%)>> Saves\storymode\%name%.SAV
::Magic
(echo spellfire1=%spellfire1%)>> Saves\storymode\%name%.SAV
(echo spellfire2=%spellfire2%)>> Saves\storymode\%name%.SAV
(echo spellfire3=%spellfire3%)>> Saves\storymode\%name%.SAV
(echo spellfire4=%spellfire4%)>> Saves\storymode\%name%.SAV
(echo spellfire5=%spellfire5%)>> Saves\storymode\%name%.SAV
(echo spellwater1=%spellwater1%)>> Saves\storymode\%name%.SAV
(echo spellwater2=%spellwater2%)>> Saves\storymode\%name%.SAV
(echo spellwater3=%spellwater3%)>> Saves\storymode\%name%.SAV
(echo spellwater4=%spellwater4%)>> Saves\storymode\%name%.SAV
(echo spellwater5=%spellwater5%)>> Saves\storymode\%name%.SAV
(echo spellwaterblast1=%spellwaterblast1%)>> Saves\storymode\%name%.SAV
(echo spellwaterblast2=%spellwaterblast2%)>> Saves\storymode\%name%.SAV
(echo spellwaterblast3=%spellwaterblast3%)>> Saves\storymode\%name%.SAV
(echo spellleafcutter1=%spellleafcutter1%)>> Saves\storymode\%name%.SAV
(echo spellleafcutter2=%spellleafcutter2%)>> Saves\storymode\%name%.SAV
(echo spellleafcutter3=%spellleafcutter3%)>> Saves\storymode\%name%.SAV
(echo spellleafcutter4=%spellleafcutter4%)>> Saves\storymode\%name%.SAV
(echo spellult1=%spellult1%)>> Saves\storymode\%name%.SAV
(echo spellult2=%spellult2%)>> Saves\storymode\%name%.SAV
(echo spellult3=%spellult3%)>> Saves\storymode\%name%.SAV
(echo spellcometcrash=%spellcometcrash%)>> Saves\storymode\%name%.SAV
ping localhost 4 >nul
echo Success!
pause
if "!room!" == "a" goto draganvillage
goto home

:runaway
cls
echo You successfully ran away!
pause >nul
goto home








