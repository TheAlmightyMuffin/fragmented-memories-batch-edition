@echo off
setlocal enabledelayedexpansion
TITLE InCom2: Project Patch 12
goto loading
:loading
cls
echo Loading, please wait.
echo _
ping localhost -n 1 >nul
cls
echo Loading, please wait.
echo.
echo :
ping localhost -n 1 >nul
cls
echo Loading, please wait.
echo.
echo _
ping localhost -n 1 >nul
cls
echo Loading, please wait.
echo.           
echo  :
ping localhost -n 1 >nul
cls
echo Loading, please wait.
echo _
echo.
ping localhost -n 1 >nul
:menu
cls
echo -------------------------------------
echo Project Patch 12
echo -----PRE-ALPHA 0.01------------------
echo.
echo.
echo 1) New Game
echo 2) Load Game
set /p letter=
if %letter% == 1 goto new
if %letter% == 2 goto load
if %letter% == call goto callfunction
goto menu
:new
cls
echo I N C O M  2 :  P R O J E C T  P A T C H  1 2
pause >nul
cls
echo It is recommended you play InCom first.
echo.
echo 	Following the events of InCom,
echo a strange boy was walking down Main Street
echo while the paperboy(who updated phones and tablets while 
echo walking down Main Street) was telling people who didn't
echo have an electronic device on them the news.
echo 	He encountered the boy.
echo 	"Hey, have you heard? The big robot that
echo destroyed Central Castle was defeated.
echo Hey, You don't look like you're from 
echo these parts. I would be cautious, there has been
echo a recent uproar in criminals. Where are you from?"
echo 	He said nothing.
echo 	"Well, I have to go."
pause
cls
:newstats
cls
::Temp
set name=Beta
::Main
set hp=13
set hpmax=13
set exp=0
set exptill=30
set money=5
set lvl=1
set pots=5
set power=4
set powergain=2
set magicgain=2
set weapon=Wooden Sword
set weaponpow=1
set weaponprice=30
set magic=5
set armor=0
set armupgrade=0
set room=0
set keys=0
set asword=0
set jewels=0
set debug=0
set pot50s=0
set final=0
set cantgetjewel=0
set smission1=0
::Party Members
set alpha=0
set alphahp=25
set ictp=1
set ictphp=10
::Magic
set spellfire1=0
set spellfire2=0
set spellfire3=0
set spellfire4=0
set spellfire5=0
set spellwater1=0
set spellwater2=0
set spellwater3=0
set spellwater4=0
set spellwater5=0
set spellwaterblast1=0
set spellwaterblast2=0
set spellwaterblast3=0
set spellleafcutter1=0
set spellleafcutter2=0
set spellleafcutter3=0
set spellleafcutter4=0
set spellult1=0
set spellult2=0
set spellult3=0
set spellcometcrash=0
echo Well, that's it for now. However,
echo you can try the battle system if you'd like
echo Chose an enemy
echo 1) Ghost
echo 2) Book
echo 3) InCom Battle1
set /p input=
if %input% == 1 goto ghost
if %input% == 2 goto book
if %input% == 3 goto incom1
:ghost
set enemyname=Ghost
set num=gh
set enghhp=10
set enghpow=3
set enghmagic=3
cls
echo A Ghost appeared
pause >nul
goto fight
:killedgh
goto newstats

:book
set enemyname=Book
set num=bk
set enbkhp=25
set enbkpow=7
set enbkmagic=0
cls
echo A Haunted Book appeared
pause >nul
goto fight
:killedbk
goto newstats

:incom1
set enemyname=InCom
set num=ic
set enichp=50
set enicpow=5
set enicmagic=7
cls
echo InCom suddenly attacked
pause >nul
goto fight
:killedic
goto newstats



::##BATTLE######################################

:fight
cls
echo %name%'s HP: %hp%
if "!alpha!" == "1" (
	echo !fgname!'s HP: !alphahp!
)
::if "!enid!" == "000" (
::	echo !enemyname!'s HP: ????
::)
::if "!enid!" neq "000"
echo.
echo 1: Attack
echo 2: Magic
echo 3: Items
echo.
echo !enemyname!'s HP: !en%num%hp!
set /p input=
if "!input!" == "1" (
	if "!alpha!" == "1" goto fight2
	goto attack
)
if %input% == 2 goto magiclist
if %input% == 3 goto inventory
:fight2
set alphaatk=0
set alphapot=0
set alphamgc=0
cls
echo %name%'s HP: %hp%
echo !fgname!'s HP: !alphahp!
echo.
echo 1: Attack
echo 2: Magic
echo.
echo !enemyname!'s HP: !en%num%hp!
set /p input=
if %input% == 1 (
	set alphaatk=1
	goto attack
	)
if %input% == 2 (
	set alphamgc=1
	goto magic
	)
:attack
cls
set /a en%num%hp=!en%num%hp! - (%power% + %weaponpow%)
if "!alphaatk!" == "1" (
	set /a en%num%hp=!en%num%hp! - %alphapow%
)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%pow!)
if %hp% leq 0 goto deathchoice
goto fight
:magiclist
cls
echo Magic
echo.
echo 1) Basic Offense
echo 2) Fire I
echo 3) Ultimate I
set /p input=
if "!inupt!" == "1" goto magicuse
if "!input!" == "2" goto usefire1
if "!input!" == "3" goto useult1
goto fight
:nomagic
cls
echo You do not have that spell...
pause
goto magiclist
:usefire1
if not "!spellfire1!" == "1" goto nomagic
cls
set /a en%num%hp=!en%num%hp! - (%magic% - 15)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:useult1
if not "!spellult1!" == "1" goto nomagic
cls
set /a en%num%hp=!en%num%hp! - (%magic% - 75)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:drinkpot
cls
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto inventory
)
set /a hp=%hp% +20
set /a pots=%pots% -1
goto fight
:drinkpot2
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto fight
)
set /a hp=%hpmax%
set /a pots=%pots% - 1
goto fight
:killed
call :killed%num%

:inventory
cls
echo Items
echo.
echo 1) Small-Potions: %pots%
echo 2) Medium-Potions: !pot50s!
echo 3) High-Potions: !pot100s!
echo 4) Mega-Potions: !pot200s!
echo 5) Max-Potions: !potmaxs!
echo 0) Back
set /p input=
if "!input!" == "1" goto drinkpot2
if "!input!" == "0" goto fight
goto fight

:deathchoice
cls
echo You were defeated...
pause >nul
goto newstats


