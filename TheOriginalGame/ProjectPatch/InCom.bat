﻿@echo off
setlocal enabledelayedexpansion
TITLE InCom
color 0A
goto loading
:loading
cls
echo.
echo This is the original game InCom that eventually evolved into FMoaLP. Please note that this version really,
echo REALLY sucks, and should not be seen by human eyes. If you want to play this, though, I'm not stopping you.
echo.
ping localhost -n 4 >nul
goto menu
:menu
cls
echo.
echo -------------------------------------
echo InCom
echo -----BETA 1.4 DEVELOPER RELEASE------
echo.
echo.
echo 1) New Game
echo 2) Load Game
echo 3) Help (Hint: type 3 for this option)
set /p letter=
if %letter% == 1 goto mode
if %letter% == 2 goto load
if "!letter!" == "3" goto firstHelp
if "!letter!" == "call" goto callfunction
goto menu
:firstHelp
cls
echo Getting started:
echo.
echo Whenever you see something like the following:
echo.
echo 1) Say Hi, and go forth
echo 2) RUN^^!^^!^^!^^!
echo.
echo you type in the number that you want. e.g. if I press 2
echo I'll run.
echo.
pause
goto home
:mode
cls
echo NEW GAME
echo.
echo Which gamemode do you want to play?
echo.
echo 1) Story Mode
echo 2) Freemode (DEMO)
::set /p gamemode=
if "!gamemode!" == "1" goto startstory
if "!gamemode!" == "2" goto startfreemode
goto startstory
:load
cls
echo LOAD GAME
echo.
echo Which gamemode do you want to play?
echo.
echo 1) Story Mode
echo 2) Freemode (DEMO)
::set /p gamemode=
if "!gamemode!" == "1" goto loadstory
if "!gamemode!" == "2" goto loadfreemode
goto loadstory
:startstory
cls
echo First create a save name then we'll continue...
echo.
set /p save=
cls
echo Once, there was a world. A world that was once a peaceful
echo place. However, Earth became no more. Luckily, a few people
echo survived and went to a distant planet. They were able to repopulate
echo the human species. They decided to make a new world with new rules
echo and new ways of life. A war started between a person who wanted
echo life to be terrible for everyone else. He had mystical powers.
echo He killed the princess. A boy with the same powers defeated him, though.
echo.
pause
cls
echo Years ago, 16 years after the war, the princess was revived.
echo But what happened two months after, she vanished. ICTP went to look
echo for her for centries, but no luck. He, ICTP, was never seen again.
echo.
pause
cls
echo NAME ENTRY
echo WHAT IS YOUR NAME?
set /p name=
cls
echo NAME ENTRY
echo WHAT IS THE EVIL LORD'S NAME?
set /p elname=
cls
echo WHAT ELEMENT ARE YOU?
echo.
echo 1: Basic
echo 2: Fire
echo 3: Water
echo 4: Grass
set /p input=
if %input% == 1 (
set element=Basic
goto new
)
if %input% == 2 (
set element=Fire
goto new
)
if %input% == 3 (
set element=Water
goto new
)
if %input% == 4 (
set element=Grass
goto new
)
:new
cls
set hp=13
set hpmax=13
set exp=0
set exptill=30
set money=5
set lvl=1
set pots=0
set power=4
set powergain=2
set magicgain=2
set weapon=Wooden Sword
set weaponpow=1
set weaponprice=30
set magic=5
set armor=0
set armupgrade=0
set room=0
set keys=0
set asword=0
set jewels=0
set debug=0
set pot50s=0
set final=0
set cantgetjewel=0
set smission1=0
::Party Members
set alpha=0
set alphahp=25
set ictp=1
set ictphp=10
::Overworld Data
set itm1=*
set itm2=*
::Magic
set spellfire1=0
set spellfire2=0
set spellfire3=0
set spellfire4=0
set spellfire5=0
set spellwater1=0
set spellwater2=0
set spellwater3=0
set spellwater4=0
set spellwater5=0
set spellwaterblast1=0
set spellwaterblast2=0
set spellwaterblast3=0
set spellleafcutter1=0
set spellleafcutter2=0
set spellleafcutter3=0
set spellleafcutter4=0
set spellthunder1=0
set spellthunder2=0
set spellthunder3=0
set spellult1=0
set spellult2=0
set spellult3=0
set spellcometcrash=0
goto home
:death
cls
echo You Died!
set /a hp=%hpmax%
pause >nul
goto home
:runaway
cls
echo You successfully ran away!
pause >nul
goto home
:levelup
if %lvl% geq 9999 goto lvlupno
cls
echo You Leveled Up
pause >nul
set /a lvl=%lvl% + 1
set /a exp=%exp% - %exptill%
set /a exptill=%exptill% + %exptill% * 66 / 100
set /a power=%power% + %powergain%
set /a hpmax=%hpmax% + 4
set /a magic=%magic% + %magicgain%
set /a room=!room! + 1
goto spell
:spell
cls
echo You learned:
if "!lvl!" == "4" (
	if "!element!" == "Basic" (
		echo It was Fire I
		echo and Water I
		set spellfire1=1
		set spellwater1=1
		pause
	)
	if "!element!" == "Water" (
		echo It was Water II
		set spellwater2=1
		pause
	)
)
call :outerkingdom!room!
:lvlupno
goto home
:loadstory
cls
set /p save=Load Name: 
if not exist Saves\storymode\!save!.SAV goto noload
if exist Saves\storymode\!save!.SAV set load=1
for /f %%a in (Saves\storymode\!save!.SAV) do set %%a
goto home
:loadfreemode
cls
set /p name=Load Name: 
if not exist Saves\freemode\!save!.SAV goto noload
if exist Saves\freemode\!save!.SAV set load=1
for /f %%a in (Saves\freemode\!save!.SAV) do set %%a
goto homefree
:noload
cls
echo !name! Does not exist!
pause >nul
goto menu
::###HOME##################################################################################################################################
:home
set gamemode=1
if "!room!" == "a" goto draganvillage
if %exp% geq %exptill% goto levelup
cls
echo !name!
echo Level !lvl!
echo.
echo Central Kingdom
echo.
echo.
echo What do you want to do?
echo.
echo 1: Outside the kingdom
echo 2: Shop
echo 3: Hospital
echo 4: Temple of Light
echo 5: Stats
echo 6: SaveStation
echo 7: Options
echo 8: Extras
echo 0: Main Menu
set /p input=
if %input% == 1 goto outerkingdom0
if %input% == 2 goto shop
if %input% == 3 goto healer
if %input% == 4 goto aswordno
if %input% == 5 goto stats
if %input% == 6 goto save
if %input% == 7 goto ingameoption
if %input% == 8 goto home2
if %input% == 0 goto menu
if %input% == 011 goto overworld1loadhome
if %input% == 9898 goto edit
:edit
cls
TITLE InCom In-Game Editor
edit InCom.bat
TITLE InCom
goto home
:ingameoption
cls
echo 1) Info
echo 2) Events
echo 3) Exit
echo 4) Back
set /p letter=
if %letter% == 1 goto info2
if %letter% == 2 goto event4
if %letter% == 3 exit
if %letter% == 4 goto home
:info2
cls
echo In Progress
pause >nul
goto ingameoption
:home2
cls
echo Yes?
echo.
echo 1: PROJECT TIME CAPSULE
echo 2: Editor
echo 3: Special Missions(does not work)
echo 4: Help Topics
echo 0: Back
set /p input=
if "!input!" == "1" goto ptc
if "!input!" == "2" goto edit
if "!input!" == "3" goto home
if "!input!" == "4" goto help
if "!input!" == "0" goto home
goto home
:help
cls
echo Help Topics
echo.
echo 1) How to play
echo 2) Dragans
echo 3) Chaos War
echo 4) Patch
echo 5) What's new
echo 0) Back
set /p input=
if "!input!" == "1" goto helphow
if "!input!" == "2" goto helpdragan
if "!input!" == "3" goto helpchaos
if "!input!" == "4" goto helppatch
if "!input!" == "5" goto helpnew
if "!input!" == "0" goto home2
:helphow
cls
echo In Progress...
pause
goto home2
:helpdragan
cls
echo Dragans were a dragon-like creature which produce oxygen
echo and trapping it around them making an oxygen bubble around
echo them.
pause
goto home2
:helpchaos
cls
echo In Progress
pause
goto home2
:helppatch
cls
echo 	Patch were a series of robots created by an
echo unknown source
echo.
echo 	They include:
echo Patch 1: Platinum
echo Patch 2: Server
echo Patch 3: Begin
echo Patch 4: 
pause
goto help
:helpnew
cls
echo InCom Public Beta "ProjectXPatch"
echo Beta 1.4
echo Coming Soon
pause
goto home2
:aswordno
cls
if %asword% geq 1 goto asword2
if %keys% geq 3 goto aswordyes
echo You need 3 keys to get in the temple...
pause >nul
goto home
:aswordyes
cls
echo You got Sword of Light!
set /a weaponpow=160
set /a weapon=Sword of Light
set /a asword=1
pause >nul
goto home
:asword2
cls
echo You don't need to be here
echo.
pause
goto home
:password1
cls
echo THE PASSWORD IS:
echo LikenOotHer12349
pause
goto keyguard1
::###SHOP##################################################################################################################################
:shop
cls
echo Hello, %name%, Welcome to my shop
echo.
echo.
echo Money: %money%
echo 1: Weapons
echo 2: Potions
echo 3: EXP Stuff
echo 4: Mechanical Arm Gold 800
echo 0: Exit
::echo 5: More
set /p option=
if %option% == 1 goto weapons
if %option% == 2 goto potions
if %option% == 3 goto expshop
if %option% == 4 goto mecharmshop
if %option% == 0 goto home
::if %option% == 5 goto moreshop
:potions
cls
echo 1: FullHP Gold 20
echo 2: MaxHpUp Gold 80
echo 3: Super-MaxHpUp Gold 500
echo 0: Back
set /p letter=
if %letter% == 1 goto buypot
if %letter% == 2 goto maxhp
if %letter% == 3 goto superhp
if %letter% == 0 goto shop
:maxhp
cls
if %money% lss 80 goto not
echo Your Max HP went up!
pause >nul
set /a money=%money% - 80
set /a hpmax=%hpmax% + 5
goto potions
:superhp
cls
if %money% lss 500 goto not
echo Your Max HP went up!
pause >nul
set /a money=%money% - 500
set /a hpmax=%hpmax% + 50
goto potions
:weapons
cls
echo 1: Sword Gold 50
echo NOT AVAILABLE, SORRY: Bow Gold 100
echo 3: WeaponUpgrade Gold %weaponprice%
echo NOT AVAILABLE, SORRY: Armor Gold 100
echo NOT AVAILABLE, SORRY: Shield Gold 50
echo NOT AVAILABLE, SORRY: Battle Suit Gold 1,000,000
echo 7: Back
set /p letter=
if %letter% == 1 goto buysword1
::if %letter% == 2 goto buybow1
if %letter% == 3 goto upgradeweapon
::if %letter% == 4 goto buyarmor
if %letter% == 7 goto shop
:upgradeweapon
if %money% lss %weaponprice% goto not
if %asword% == 1 goto notup
if %weaponpow% leq 0 gotonotup
if %weaponpow% geq 140 goto notup
set /a money=%money% - %weaponprice%
set /a weaponpow=%weaponpow% + 4
set /a weaponprice=%weaponprice% + 17
cls
echo Successfuly upgraded...
pause >nul
goto shop
:buysword1
if %money% lss 50 goto not
if %asword% == 1 goto notbuy
set /a money=%money% - 50
set /a weaponpow=14
set /a weapon=Sword
cls
echo Successfuly bought
pause >nul
goto shop
:buybow1
if %money% lss 50 goto not
if %asword% == 1 goto notbuy
set /a money=%money% - 100
set /a weaponpow=45
set /a weapon=Bow
cls
echo Successfuly bought
pause >nul
goto shop
:buyarmor
if %money% lss 100 goto not
set /a money=%money% - 100
set /a armor=50
cls
echo Successfuly bought
pause >nul
goto shop
:notup
echo I can't upgrade that!
pause >nul
goto shop
:notbuy
echo Use your weapon instead!
pause >nul
goto shop
:buypot
if %money% lss 20 goto not
set /a pots=%pots% + 1
set item!itemsmax!=Potion
set /a money=%money% - 20
goto shop
:expshop
cls
echo 1: EXP Orbs Gold 5
echo 2: Instant Level Up Gold 50
echo 3: Back
set /p letter=
if %letter% == 1 goto orbshop
if %letter% == 2 goto levelshop
if %letter% == 3 goto shop
:orbshop
if %money% lss 5 goto not
set /a exp=%exp% + 2
set /a money=%money% - 5
goto shop
:mecharmshop
if %armupgrade% geq 1 goto noarm
if %money% lss 800 goto not
set /a power=%power% + 320
set /a money=%money% - 800
set armupgrade=1
goto shop
:levelshop
if %money% lss 50 goto not
if %lvl% geq 100 goto lvlupno
echo You Leveled Up
pause >nul
set /a money=%money% - 50
set /a lvl=%lvl% + 1
if %power% geq 120(set /a power=120)
set /a power=%power% + %powergain%
set /a hpmax=%hpmax% + 4
set /a magic=%magic% + 4
goto shop
:not
cls
echo You don't have enough money!
pause >nul
goto shop
:noarm
cls
echo You already have a Mechanical Arm!
pause >nul
goto shop
:healer
cls
echo Welcome, %name%
echo Do you want to get healed?
echo.
echo 1) Yes
echo 2) No
set input=2
set /p input=...
if %input% equ 1 goto healer2
goto home
:healer2
set hp=%hpmax%
goto home
::###THE WORLD##########################################################################################################################
:outerkingdom0
if %room% == 0 goto outerkingdom
if %room% == 1 goto outerkingdom1
if %room% == 2 goto outerkingdom2l
if %room% == 3 goto outerkingdom3
if %room% == 4 goto outerkingdom4
if %room% == 5 goto outerkingdom5
if %room% == 6 goto outerkingdom6
if %room% == 7 goto outerkingdom7
if %room% == 9 goto outerkingdom9
if %room% == 10 goto outerkingdom10
if %room% == 11 goto outerkingdom11
if %room% == 12 goto outerkingdom12
if %room% == 13 goto outerkingdom13
if %room% == 14 goto outerkingdom14
if %room% == 15 goto outerkingdom15
if %room% == 16 goto outerkingdom16
if %room% == 17 goto outerkingdom17
if %room% == 18 goto outerkingdom18
if %room% == 19 goto outerkingdom19
if %room% == 20 goto outerkingdom20
if %room% == 21 goto outerkingdom21
if %room% == 22 goto outerkingdom22
if %room% == 23 goto outerkingdom23
if %room% == 24 goto outerkingdom24
if %room% == 25 goto outerkingdom25
if %room% == 26 goto outerkingdom26
if %room% == 27 goto outerkingdom27
if %room% == 28 goto outerkingdom28
if %room% == 29 goto outerkingdom29
if %room% == 30 goto outerkingdom30
if %room% == 31 goto outerkingdom31
if %room% == 32 goto outerkingdom32
if %room% == 33 goto outerkingdom33
if %room% == 33 goto outerkingdom33
if %room% == 34 goto outerkingdom34
if %room% == 35 goto outerkingdom35
if %room% == 36 goto outerkingdom36
if %room% == 37 goto outerkingdom37
if %room% == 38 goto outerkingdom38
if %room% == 39 goto outerkingdom39
if %room% == 40 goto outerkingdom40
if %room% == end2 goto almost2end2
if %room% == chaos goto chaos1
:overworld
cls
:outerkingdom
set /a room=0
set enemyname=Ghost
set enghosthp=10
set num=ghost
set enghostpow=3
set enghostmagic=3
set en1hp=10
cls
echo A Ghost appeared
pause >nul
goto fight
:killedghost
cls
echo You defeated the ghost
echo you got 8 EXP
echo and Gold 3
set /a exp=%exp% + 8
set /a money=%money% + 3
pause >nul
set /a en1hp=10
goto outerkingdom1
:outerkingdom1
set /a room=1
if "!exp!" geq "!exptill!" goto levelup
cls
echo What do you want to do?
echo.
echo 1: Go forward
echo 2: Go left
echo 3: Go right
echo 4: Go back
echo 5: Go teleport home
set /p letter=
if %letter% == 1 goto outerkingdom2
if %letter% == 2 goto outerkingdom2l
if %letter% == 3 goto outerkingdom2r
if %letter% == 4 goto outerkingdom1
if %letter% == 5 goto home
:outerkingdom2
cls
echo Rock Golem appeared
pause >nul
set enemyname=Rock Golem
set num=rg
set enrghp=20
set enrgpow=5
set enrgmagic=6
goto fight
:killedrg
cls
echo You defeated the Rock Golem
echo you got 16 EXP
echo and 10Gold 
set /a exp=%exp% + 16
set /a money=%money% + 10
set killedzombie=0
pause >nul
set /a en2hp=20
goto outerkingdom2
:outerkingdom2
set /a room=2
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go left
echo 3: Go back
echo 4: Go teleport home
set /p letter=
if %letter% == 1 goto outerkingdom3
if %letter% == 2 goto outerkingdom3l
if %letter% == 3 goto outerkingdom
if %letter% == 4 goto home
:outerkingdom2r
set /a room=2
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go left
echo 3: Go back
echo 4: Go teleport home
set /p letter=
if %letter% == 1 goto outerkingdom3
if %letter% == 2 goto outerkingdom3l
if %letter% == 3 goto outerkingdom2
if %letter% == 4 goto home
:outerkingdom3
set /a room=3
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto outerkingdom4
if %letter% == 2 goto outerkingdom2
if %letter% == 3 goto home
:outerkingdom3l
cls
echo Zombie appeared
set enemyname=Zombie
set num=zb
set enzbhp=11
set enzbpow=3
set enzbmagic=4
pause >nul
goto fight
:killedzb
cls
echo You defeated the Zombie
echo you got 5 EXP
echo and Gold 3
set /a exp=%exp% + 5
set /a money=%money% + 3
set zkilled=1
pause >nul
set /a room=4
if "!exp!" geq "!exptill!" goto levelup
set /a en1hp=10
goto outerkingdom4
:outerkingdom4
set /a room=4
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go left
echo 3: Go right
echo 4: Go back
echo 5: Go teleport home
set /p letter=
if %letter% == 1 goto boss1
if %letter% == 2 goto outerkingdom4
if %letter% == 3 goto outerkingdom4
if %letter% == 4 goto outerkingdom3
if %letter% == 5 goto home
:boss1
if "!zkilled!" == "0" goto outerkingdom5
cls
echo the Iron Golem Boss appeared
set enemyname=Iron Golem
set num=iron
set enironhp=40
set enironpow=9
set enironmagic=9
pause >nul
goto fight
:killediron
cls
echo You defeated the Iron Golem
echo you got 18 EXP and
echo Gold 27
set /a exp=%exp% + 18
set /a money=%money% + 27
echo Also one of the rare jewels.
set /a jewels=%jewels% + 1
pause >nul
set /a en4hp=40
goto outerkingdom5
:outerkingdom5
set /a room=5
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go left
echo 3: Go right
echo 4: Go back
echo 5: Go teleport home
set /p letter=
if %letter% == 1 goto outerkingdom6
if %letter% == 2 goto outerkingdom6
if %letter% == 3 goto outerkingdom6
if %letter% == 4 goto outerkingdom5
if %letter% == 5 goto home
:outerkingdom6
set /a room=6
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go left
echo 3: Go right
echo 4: Go back
echo 5: Go teleport home
set /p letter=
if %letter% == 1 goto bossOBotProto
if %letter% == 2 goto bossOBotProto
if %letter% == 3 goto bossOBotProto
if %letter% == 4 goto outerkingdom5
if %letter% == 5 goto home
:bossOBotProto
cls
echo the OBot Prototype boss appeared
set enemyname=OBot Proto
set num=obpr
set enobprhp=40
set enobprpow=9
set enobprmagic=9
pause >nul
goto fight
:killedobpr
cls
echo You defeated the Iron Golem
echo you got 15 EXP and
echo Gold 25
set /a exp=%exp% + 15
set /a money=%money% + 25
pause >nul
set /a en4hp=40
:outerkingdom7
set /a room=7
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go left
echo 3: Go right
echo 4: Go back
echo 5: Go teleport home
set /p letter=
if %letter% == 1 goto story1
if %letter% == 2 goto story1
if %letter% == 3 goto story1
if %letter% == 4 goto outerkingdom5
if %letter% == 5 goto home
:story1
cls
if "!talk6!" == "1" goto outerkingdom9
echo ICTP: Woah, is it really?
echo ICTP: It looks like the Temple of Light
echo ICTP: Back in 4306, NASA of Earth found new planets that supported life.
echo.
pause >nul
cls
echo ICTP: They had seperated the new planets time into Time Periods.
echo The Time Periods were Modern, Middle Future, and SuperFuture
echo ICTP: One of the planets wanted control over time, but to do that,
echo they must destroy our planet.
echo ICTP: Our planet is a superior planet.
echo.
pause >nul
echo ICTP: The queen of that planet has her son !elname!
echo trying to destroy our planet.
echo We need the Platinum Keys of Grass, Water, and Fire to get in.
echo.
pause >nul
set talk6=1
goto outerkingdom8
:outerkingdom8
set /a room=9
cls
echo What would you like to do
echo.
echo 1) Go Forward
echo 2) Teleport home
echo.
set /p input=
if "!input!" == "2" goto home
:outerkingdom9
set /a room=9
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto goblin
if %letter% == 2 goto outerkingdom7
if %letter% == 3 goto home
:goblin
echo 101error
set enemyname=101error
set num=101
set en101pow=11
set en101magic=9
set en101hp=27
pause >nul
goto fight
:killed101
cls
echo You defeated the... 101error?
echo You got nothing.
pause >nul
goto outerkingdom10
:outerkingdom10
set /a room=10
cls
echo 10TH ROOM!!!!!
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto boss2
if %letter% == 2 goto outerkingdom9
if %letter% == 3 goto home
:boss2
set enemyname=Grass Key Guardian
set num=b2
set enb2hp=80
set enb2pow=15
set enb2magic=16
cls
echo ICTP: There is the 1st Key!
echo.
echo the Grass Key Guardian Boss appeared!
pause >nul
goto fight
:killedb2
cls
echo You defeated the Guardian!
echo you got 100 EXP,
echo Gold 70
echo and the Grass Key!
set /a exp=%exp% + 100
set /a money=%money% + 70
set /a keys=1
pause >nul
set /a boss2hp=80
goto outerkingdom11
:outerkingdom11
set /a room=11
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto outerkingdom12
if %letter% == 2 goto outerkingdom10
if %letter% == 3 goto home
:outerkingdom12
set /a room=12
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto boss3
if %letter% == 2 goto outerkingdom11
if %letter% == 3 goto home
:boss3
cls
set enemyname=Water Key Guardian
set num=b3
set enb3hp=100
set enb3pow=15
set enb3magic=15
echo ICTP: The second one, already
echo.
echo the Water Key Guardian Boss appeared!
echo.
pause >nul
goto fight
:killedb3
cls
echo You defeated the Guardian!
echo you got 120 EXP,
echo Gold 90
echo and the Water Key!
set /a exp=%exp% + 120
set /a money=%money% + 90
set /a keys=2
pause >nul
set /a boss3hp=100
goto outerkingdom13
:outerkingdom13
set /a room=13
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto keyguard1
if %letter% == 2 goto outerkingdom12
if %letter% == 3 goto home
:keyguard1
cls
echo 3rd Key Guardian: Ha! You need a password to get in here!
echo 3rd Key Guardian: You'll never find the password, those
echo other guardians were stupid!
echo.
set /p password=PASSWORD: 
::LikenOotHer12349::
if %password% == pass goto password1
if %password% == LikenOotHer12349 goto keyguard2
if not %password% == LikenOotHer12349 goto passwordno1
if not %password% == pass goto passwordno1
goto passwordno1
:passwordno1
echo NO!
pause >nul
goto keyguard2
:keyguard2
cls
set enemyname=OBot
set enid=obot
set enobothp=80
set enobotpow=10
set enobotmagic=10
echo Metal OBot appeared
pause >nul
goto fightobot
:killedobot
cls
echo You defeated OBot
echo you got 20 EXP
echo and 13Gold 
set /a exp=%exp% + 16
set /a money=%money% + 10
pause >nul
set /a en3hp=30
goto keyguard3
:keyguard3
cls
set enemyname=OBot
set enid=obot
set enobothp=80
set enobotpow=10
set enobotmagic=10
echo Metal OBot appeared
pause >nul
goto fightobot
:killedobot
cls
echo You defeated OBot
echo you got 20 EXP
echo and 13Gold 
set /a exp=%exp% + 16
set /a money=%money% + 10
pause >nul
set /a en3hp=30
goto keyguard3
:keyguard4
set enemyname=Fire Key Guardian
set num=b4
set enfkghp=110
set enfkgpow=20
set enfkgmagic=18
cls
echo Fire Key Guardian appeared
pause >nul
goto fight
:fightboss4
cls
echo %name%'s HP: %hp%
echo Key Guardian's HP: %boss4hp%
echo.
echo 1: Attack
echo 2: Magic
echo 3: Potions
set input=5
set /p input=
if %input% == 1 goto attackboss4
if %input% == 2 goto magicboss4
if %input% == 3 goto drinkpotboss4
if %boss4hp% leq 0 goto killedboss4
:attackboss4
cls
set /a boss4hp=%boss4hp% - (%power% + %weaponpow%)
if %boss4hp% leq 0 goto killedboss4
set /a hp=%hp% - (20)
if %hp% leq 0 goto death
goto fightboss4
:magicboss4
cls
set /a boss4hp=%boss4hp% - %magic%
if %boss4hp% leq 0 goto killedboss4
set /a hp=%hp% - (18)
if %hp% leq 0 goto death
goto fightboss4
:drinkpotboss4
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto fightboss4
)
set /a hp=%hpmax%
set /a pots=%pots% -1
goto fightboss4
:killedb4
cls
echo You defeated the Guardian!
echo you got 140 EXP,
echo Gold 200
echo and the Fire Key!
set /a exp=%exp% + 140
set /a money=%money% + 200
set /a keys=3
pause >nul
set /a boss4hp=110
cls
if "!element!" == "Basic" (
	set /a element=Ultimate
	set spellult1=1
	echo Having all three keys has upgraded you to Ultimate Element!
	echo You learned the spell Ultimate I
	set spellultimate1=1
	pause
	)
goto outerkingdom14
:outerkingdom14
set /a room=15
cls
echo ICTP: We should got to the temple!
pause
goto home
:outerkingdom15
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto outerkingdom16
if %letter% == 2 goto outerkingdom15
if %letter% == 3 goto home
:outerkingdom16
set /a room=16
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto outerkingdom17
if %letter% == 2 goto outerkingdom15
if %letter% == 3 goto home
:outerkingdom17
set /a room=17
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto boss5
if %letter% == 2 goto outerkingdom16
if %letter% == 3 goto home
if %letter% == 4 goto warp1
:boss5
cls
echo the OBot2 appeared!
pause >nul
goto fightboss5
:fightboss5
cls
echo %name%'s HP: %hp%
echo OBot2's HP: %boss5hp%
echo.
echo 1: Attack
echo 2: Magic
echo 3: Potions
set input=5
set /p input=
if %input% == 1 goto attackboss5
if %input% == 2 goto magicboss5
if %input% == 3 goto drinkpotboss5
if %boss1hp% leq 0 goto killedboss5
:attackboss5
cls
set /a boss5hp=%boss5hp% - (%power% + %weaponpow%)
if %boss5hp% leq 0 goto killedboss5
set /a hp=%hp% - (22)
if %hp% leq 0 goto death
goto fightboss5
:magicboss5
cls
set /a boss5hp=%boss5hp% - %magic%
if %boss5hp% leq 0 goto killedtboss5
set /a hp=%hp% - (22)
if %hp% leq 0 goto death
goto fightboss5
:drinkpotboss5
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto fightboss5
)
set /a hp=%hpmax%
set /a pots=%pots% -1
goto fightboss5
:killedboss5
cls
echo You defeated OBot2
echo you got 200 EXP
echo and Gold 170
set /a exp=%exp% + 200
set /a money=%money% + 170
pause >nul
set /a boss5hp=400
goto learnspell1
:learnspell1
cls
echo You learnded new magic...
echo.
if "!element!" == "Ultimate" (
	echo It was Ultimate II
	set spellult2=1
)
if "!element!" == "Fire" (
	echo It was Fire III
	set spellfire3=1
)
if "!element!" == "Water" (
	echo It was Water Blast II
	set spellwaterblast2=1
)
if "!element!" == "Grass" (
	echo It was Leaf Cutter III
	set spellleafcutter3=1
)
:outerkingdom18
set /a room=18
if "!talk!" == "1" goto outerkingdom19
cls
echo ????:I see you have defeated the OBot2,
echo we should escape.
echo !name!: Escape where, and who are you?
pause
cls
echo NAME ENTRY
echo A friend of yours
echo.
set /p frname=NAME: 
cls
echo !frname!: It's me, !frname!.
echo The planet's acually falling apart.
echo I've unleash.. er, tried to defeat !elname!.
echo I have found a dragan, we will use that to escape to
echo another planet.
pause >nul
cls
echo !frname!: Will you escape with me?
echo.
echo 1) Yes
echo 2) No
set /p letter=
if %letter% == 1 goto end1
if %letter% == 2 goto friendno
:end1
cls
echo Okay...
echo.
echo THE END...
ping localhost -n 2 >nul
echo Or not
pause
:fightobot3
cls
echo OBot 3 Attacked
set enemyname=OBot3
set eno3hp=300
set num=o3
set eno3pow=58
set eno3magic=46
pause
goto fight
:killedo3
cls
echo You defeated OBot3
echo you got 230 EXP
echo and Gold 150
set /a exp=%exp% + 230
set /a money=%money% + 150
pause >nul
goto outerkingdom19
:friendno
cls
echo !frname!: Well, then.
echo !frname! flys away,
echo alone.
pause >nul
set talk=1
goto boss6
:boss6
cls
echo the Steel Golem Boss appeared!
pause >nul
goto fightboss6
:fightboss6
cls
echo %name%'s HP: %hp%
echo Steel Golem's HP: %boss6hp%
echo.
echo 1: Attack
echo 2: Magic
echo 3: Potions
set input=5
set /p input=
if %input% == 1 goto attackboss6
if %input% == 2 goto magicboss6
if %input% == 3 goto drinkpotboss6
if %boss6hp% leq 0 goto killedboss6
:attackboss6
cls
set /a boss6hp=%boss6hp% - (%power% + %weaponpow%)
if %boss6hp% leq 0 goto killedboss6
set /a hp=%hp% - (22)
if %hp% leq 0 goto death
goto fightboss6
:magicboss6
cls
set /a boss6hp=%boss6hp% - %magic%
if %boss6hp% leq 0 goto killedtboss6
set /a hp=%hp% - (22)
if %hp% leq 0 goto death
goto fightboss6
:drinkpotboss6
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto fightboss6
)
set /a hp=%hpmax%
set /a pots=%pots% -1
goto fightboss6
:killedboss6
cls
echo You defeated the Steel Golem
echo you got 250 EXP
echo and Gold 200
set /a exp=%exp% + 250
set /a money=%money% + 200
echo And one of the rare jewels
set /a jewels=!jewels! + 1
pause >nul
set /a boss6hp=400
goto outerkingdom19
:outerkingdom19
set /a room=19
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto outerkingdom20
if %letter% == 2 goto outerkingdom17
if %letter% == 3 goto home
:outerkingdom20
set /a room=20
cls
echo 20th ROOM!
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto skeleton
if %letter% == 2 goto outerkingdom18
if %letter% == 3 goto home
:skeleton
cls
echo a skeleton appeared!
pause >nul
set enemyname=Skeleton
set num=sk
set enskpow=38
set enskmagic=37
set enskhp=110
goto fight
:killedsk
cls
echo You defeated the skeleton
echo You got 140 exp
echo and Gold 100
pause >nul
set /a exp=!exp! + 140
set /a money=!money! + 100
goto outerkingdom21
:outerkingdom21
set /a room=21
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto outerkingdom22
if %letter% == 2 goto outerkingdom18
if %letter% == 3 goto home
:outerkingdom22
set /a room=22
cls
echo What would you like to do
echo.
echo 1: Go forward
echo 2: Go back
echo 3: Go teleport home
set /p letter=
if %letter% == 1 goto outerkingdom23
if %letter% == 2 goto outerkingdom20
if %letter% == 3 goto home
:outerkingdom23
cls
echo Thanks For Playi-
echo.
echo !elname!:I see you made it to my empire.
echo You can't defeat my army,
echo but if you defeat them, I'll teleport you to me and we will fight.
pause >nul
goto evilsarmy
:evilsarmy
cls
echo !elname!'s army appeared
set enemyname=!elname!'s Army
set num=da
set endahp=800
set endapow=70
set endamagic=0
pause >nul
goto fight
:killedda
cls
echo You defeated !elname!'s army
echo you got 1,200 EXP
echo and 1,000Gold 
if %hp% == %hpmax% (
	echo Also a jewel!
	set /a jewels=%jewels% + 1
)
set /a exp=%exp% + 1200
set /a money=%money% + 1000
pause >nul
goto evil
:evil
cls
echo !elname!: That is the FINAL straw!
echo Huh, is that the Sword of Light,
echo but, how? I thought he was...
echo No matter, I bet you will run away
echo.
echo Fight him? (y/n)
set /p input=
if "!input!" == n goto almostend1nofight
pause >nul
goto fightalpha
:fightalpha
cls
echo %name%'s HP: %hp%
echo !elname!'s HP: %boss8hp%
echo.
echo 1: Attack
echo 2: Magic
echo 3: Potions
set input=5
set /p input=
if %input% == 1 goto attackevil
if %input% == 2 goto magicevil
if %input% == 3 goto drinkpotevil
if %boss8hp% leq 0 goto killedevilsarmy
:attackevil
cls
set /a boss8hp=%boss8hp% - (%power% + %weaponpow%)
if %boss8hp% leq 0 goto killedevil
set /a hp=%hp% - (100)
if %hp% leq 0 goto death
goto fightalpha
:magicevil
cls
set /a boss8hp=%boss8hp% - %magic%
if %boss8hp% leq 0 goto killedtevil
set /a hp=%hp% - (105)
if %hp% leq 0 goto death
goto fightalpha
:drinkpotevil
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto fightevil
)
set /a hp=%hpmax%
set /a pots=%pots% -1
goto fightalpha
:killedevil
cls
echo You defeated !elname!
echo you got 1,500 EXP
echo and 1,400Gold 
set /a exp=%exp% + 1500
set /a money=%money% + 1400
pause >nul
goto almostend1
:almostend1
if %talk% == 1 goto outerkingdom24
cls
echo !elname!: I was not being smart, was I.
echo Well, you won. bu- *faint*
echo %name%: We did it! Huh?
echo Where are you going?
echo ICTP: I have to go,
echo but we'll be together soon!
echo Bye. You should get to your
echo kingdom.
pause >nul
cls
echo ????: I'm not down yet.
echo ????: One more to go.
echo ????: I'll be waiting,
echo ICTP: Who ar-ZZZZZZZ *off*
echo ????: At the Central Castle.
echo You see the creature destroy the castle
pause >nul
cls
echo NAME ENTRY
echo the mysterious evil creature
set /p ecname=
set talk7=1
goto outerkingdom24
:almostend1nofight
if %talk% == 2 goto outerkingdom24
cls
echo !elname!: Ha! I knew it.
echo Come on, get out of here.
pause >nul
cls
echo ????: I'm not down yet.
echo ????: One more to go.
echo ????: I'll be waiting,
echo ICTP: Who ar-ZZZZZZZ *off*
echo ????: At the Central Castle.
echo You see the creature destroy the castle
pause >nul
cls
echo NAME ENTRY
echo the mysterious evil creature
set /p ecname=
set talk7=2
goto outerkingdom24
:outerkingdom24
cls
set /a room=24
echo !elname!'s Land
echo.
echo 1) Go on
echo 2) Teleport home
set /p input=
if "!input!" == "1" goto outerkingdom25
if "!input!" == "2" goto home
:outerkingdom25
cls
set /a room=25
echo !elname!'s Land
echo.
echo 1) Go on
echo 2) Go back
echo 3) Teleport home
set /p input=
if "!input!" == "1" goto fightyesman
if "!input!" == "2" goto outerkingdom24
if "!input!" == "3" goto home
:: Okay, I said that the new battle system will come in Beta 1.4,
:: but what choice do I have?
:fightyesman
cls
echo the Yes Man appeared!
pause >nul
set enemyname=YesMan
set num=ym
set enympow=115
set enymmagic=116
set enymhp=612
goto fight
:killedym
cls
echo You defeated the Yes Man^^!
echo You got 230 Experience
echo and Gold 40
pause >nul
set /a exp=!exp! + 230
set /a money=!money! + 230
goto outerkingdom26
:outerkingdom26
cls
set /a room=26
echo !elname!'s Land
echo.
echo 1) Go on
echo 2) Go back
echo 3) Teleport home
set /p input=
if "!input!" == "1" goto outerkingdom27
if "!input!" == "2" goto outerkingdom25
if "!input!" == "3" goto home
:outerkingdom27
cls
set /a room=27
echo !elname!'s Land
echo.
echo 1) Go on
echo 2) Go back
echo 3) Teleport home
set /p input=
if "!input!" == "1" goto trip1
if "!input!" == "2" goto outerkingdom26
if "!input!" == "3" goto home
:trip1
if "!talk2!" == "1" goto outerkingdom28
cls
echo You trip on a cliff, but a girl helped you.
echo.
echo ????: What is it?
echo !name!: Huh?
echo That's the thing that destroyed the Central Castle^^!
echo ????: Oh...
echo !name!: By the way, what is your name?
pause
cls
echo NAME ENTRY
echo What is the girl's name
set /p fgname=
set talk2=1
cls
echo !fgname!: Oh, it's !fgname!.
echo !name!: Nice meeting you, !fgname!, but I have to go save the kingdom^^!
echo !fgname!: I come along too.
pause
cls
echo !fgname! joined your team!
set alpha=1
set alphalvl=3
set alphahp=50
set alphaatk=40
set alphamgc=10
set alphaexp=60
set alphaexptill=80
goto outerkingdom28
:outerkingdom28
cls
set /a room=28
echo !elname!'s Land
echo.
echo 1) Go on
echo 2) Go back
echo 3) Teleport home
set /p input=
if "!input!" == "1" goto outerkingdom29
if "!input!" == "2" goto outerkingdom27
if "!input!" == "3" goto home
:outerkingdom29
cls
set /a room=29
echo What to do?
echo.
echo 1) Go on
echo 2) Go back
echo 3) Teleport home
set /p input=
if "!input!" == "1" goto draganvillage
if "!input!" == "2" goto outerkingdom27
if "!input!" == "3" goto home
:draganvillage
set room=a
if %exp% geq %exptill% goto levelup
cls
echo !name!
echo Level %lvl%
echo.
echo Dragan Village
echo.
echo.
echo What do you want to do?
echo.
echo 1: Outside the village
echo 2: Shop
echo 3: Heal
echo 4: Stats
echo 5: Save
echo 0: Go Back
set /p input=
if %input% == 1 goto outerkingdom30
if %input% == 2 goto shopd
if %input% == 3 goto healerd
if %input% == 4 goto statsd
if %input% == 5 goto save
if %input% == 0 goto outerkingdom29
:shopd
cls
echo Sorry, we're out of business.
pause
goto draganvillage
:healerd
cls
echo Welcome, %name%
echo Do you want to get healed?
echo.
echo 1) Yes
echo 2) No
set input=2
set /p input=
if %input% equ 1 goto healer2d
goto draganvillage
:healer2d
set hp=%hpmax%
goto draganvillage
:statsd
cls
echo %name% is Level %lvl%
echo %exp%/%exptill% Experience
echo %hp%/%hpmax% Health
echo %money% Money
echo %pots% Potions
echo.
echo Your power is %power%
echo Your weapon power is %weaponpow%
echo Your magic power is %magic%
echo Your element is %element%
echo.
echo PRESS ANY KEY
pause >nul
goto draganvillage
:outerkingdom30
cls
set /a room=30
echo 30TH ROOM
echo What to do?
echo.
echo 1) Go on
echo 2) Go back
echo 3) Teleport home
set /p input=
if "!input!" == "1" goto timemachine1
if "!input!" == "2" goto draganvillage
if "!input!" == "3" goto home
:timemachine1
cls
if "!talk3!" == "1" goto timemachine
echo !name!: Forward Mountains? We're here already?^^!
echo !name!: Whats that?
echo !fgname!: It's a time machine^^!
echo Maybe we can go back in time and stop !ecname! from existing^^!
echo !name!: ...
pause
set talk3=1
goto timemachine

:timemachine
cls
echo What time do you want to travel to?
echo.
echo 1) 4403 - Chaos War
echo 2) 4408 - Chaos Ending
echo 0) Back
set /p input=
if "!input!" == "1" goto chaos
if "!input!" == "2" goto chaosend
if "!input!" == "3" goto revive
if "!input!" == "4" goto evilrevive
if "!input!" == "0" (
	if "!room!" == "chaos" goto chaos1
	if "!room!" == "chaosend" goto chaosend1
	goto outerkingdom30
)
:chaos
cls
if "!talk4!" == "1" (
	echo You traveled to the Central Castle in 4403
		pause >nul
		goto chaos1
)
echo The time machine wont turn on!
pause >nul
goto talk4
:chaos1
cls
set room=chaos
echo Central Castle - 4403
echo F1
echo.
echo 1) Go on
echo 2) Go back
set /p input=
if "!input!" == "1" goto chaos2
if "!input!" == "2" goto timemachine
:chaos2
cls
echo Central Castle
echo F2
echo.
echo 1) Go on
echo 2) Go back
set /p input=
if "!input!" == "1" goto chaos3talk
if "!input!" == "2" goto chaos1
:chaos3talk
if "!talk5!" == "1" goto chaos3
cls
echo ????: Who's there?
echo !name!: ...
echo ????: What's your name?
echo !name!: ..... !name!.
pause
cls
echo NAME ENTRY
echo The boy who ended the war
set /p awname=
cls
echo ????: Oh, sorry, I forgot to mention my name^^!
echo !awname!: It's !awname!.
echo !name!: Hello, !awname!^^!
echo !awname! joined your team^^!
pause
cls
echo !fgname!: What did I miss?
echo !awname!: Who are you?
echo !fgname!: It's !fgname!.
echo !awname!: Well, my name is !awname!.
echo !name!: Well, let's go defeat !ecname!^^!
pause >nul
set talk5=1
goto chaos3
:chaos3
cls
echo Central Castle
echo F3
echo.
echo 1) Go on
echo 2) Go back
set /p input=
if "!input!" == "1" goto chaos4
if "!input!" == "2" goto chaos2
:chaos4
cls
echo Central Castle
echo F4
echo.
echo 1) Go on
echo 2) Go back
set /p input=
if "!input!" == "1" goto friendpresent
if "!input!" == "2" goto chaos3
:friendpresent
cls
echo !frname!: I wonder if he's safe.
echo I'm going back.
pause
goto chaos5
:chaos5
cls
echo Central Castle
echo F5
echo.
echo 1) Go on
echo 2) Go back
set /p input=
if "!input!" == "1" goto chaos6
if "!input!" == "2" goto chaos4no
:chaos4no
cls
echo The stairs are blocked!
pause
goto chaos5
:chaos6
cls
echo Central Castle
echo F6
echo.
echo 1) Go on
echo 2) Go back
set /p input=
if "!input!" == "1" goto metalgolem
if "!input!" == "2" goto chaos5
:metalgolem
cls
echo A metal golem appeared^^!
pause >nul
set enemyname=Metal Golem
set num=mg
set enmgpow=184
set enmgmagic=190
set enmghp=870
goto fight
:killedmg
cls
echo You defeated the metal golem^^!
echo You got 450 exp
echo and Gold 300
pause >nul
set /a exp=!exp! + 450
set /a money=!money! + 300
goto chaos7
:chaos7
cls
echo Central Castle
echo F7
echo.
echo 1) Go on
echo 2) Go back
set /p input=
if "!input!" == "1" goto chaos8
if "!input!" == "2" goto chaos6

:chaos8
cls
echo Central Castle
echo F8
echo.
echo 1) Go on
echo 2) Go back
set /p input=
if "!input!" == "1" goto chaos9
if "!input!" == "2" goto chaos7

:chaos9
cls
echo Central Castle
echo F9
echo.
echo 1) Go on
echo 2) Go back
set /p input=
if "!input!" == "1" goto top
if "!input!" == "2" goto chaos8
:top
cls
echo Central Castle
echo Rooftop
echo.
echo 1) Go on
echo 2) Go back
set /p input=
if "!input!" == "1" goto flydragan
if "!input!" == "2" goto chaos9
:flydragan
cls
echo !name!: !awname!, hold on tight^^!
echo !awname!: There it is, !ecname!
echo You and !awname! fly off to defeat !ecname!,
echo but where are ICTP and !elname!^^!
pause
cls
echo !frname!: A computer^^!
echo Let's see if it works.
echo ICTP: *on* BOOTING INCOM INTERFACE...
echo BOOTING INCOM UI...
echo BOOTING INCOM OS...
echo DONE.
echo ICTP: Where's !name!^^!
echo I thought he was with me^^!
echo Oh, a time machine.
echo ICTP travels to 4356
echo !frname!: Oh, well, I should go with it.
pause
goto draganfinal
:draganfinal
cls
echo !name!: Everyone came here to support me.
echo ICTP, you're okay^^!
echo !awname!: Everybody, on a dragan^^!
echo !name!/!awname!: Let's do this^^!
pause
goto fightfdragan1
:fightfdragan1
cls
echo !ecname! attacked^^!
pause
set enemyname=!ecname!
set num=ec
set enecpow=620
set enecmagic=670
set enechp=6000
goto fight
:killedec
cls
echo ICTP: !ecname! wasn't evil, it was 
echo mutated and controlled.
echo !frname!: !ecname! was a mutated dragan, controled by the queen
echo of !elname!'s planet.
echo !ecname!: You fools^^!
pause
goto fightfdragan2
:fightfdragan2
cls
echo !ecname! attacked again!
pause
set enemyname=!ecname!
set num=ec2
set enec2pow=987
set enec2magic=1000
set enec2hp=8000
goto fight
:killedec2
cls
echo !ecname!: RAWR^^!^^!^^!
pause
goto fightfdragan3
:fightfdragan3
cls
echo !ecname! attacked angerly^^!
pause
set enemyname=!ecname!
set num=ec3
set enec3pow=1396
set enec3magic=1397
set enec3hp=14000
goto fight
:killedec3
cls
echo !ecname!: No, NO
pause
goto fightdraganfinal
:fightdraganfinal
cls
echo !ecname! attacked in its hyper mode^^!
pause
set enemyname=!ecname!
set num=ec4
set enec4pow=1756
set enec4magic=1758
set enec4hp=17000
goto fight
:killedec4
cls
echo !ecname!: NOOOOOOOOO^^!
echo !ecname! turned into a regular dragan, fainted.
echo !elname!: Well, i hate to admit it, but
echo that was a pretty cool battle there.
echo !name!: You're still alive?
echo !elname!: Yea, but I'm not evil any more.
echo Everyone: HURRAAAAAAAAAAY^^!
echo ICTP: But, why are you not evil anymore?
echo !elname!: Well, after you defeated me, I was
echo tired of getting beat up like that.
echo !frname!: So because you don't want your butt kicked anymore,
echo you stoped being evil?
echo !elname!: Yup.
echo !fgname!: Well enough talking, let's party^^!
echo !name!/!awname!/!elname!: Party?
echo !kgname!: How 'bout we party in the present.
echo Everyone: YEAH
pause >nul
cls
echo One party later...
pause >nul
cls
echo !name!: Bye, !awname!.
echo !awname!: Bye, !name!.
echo !name!: We'll miss you.
echo !awname!: Okay, bye.
pause
if "!jewels!" geq "3" goto almostend2
cls
start thank.vbs
set room=0
pause
cls
echo Would you like to save?
echo.
echo 1) Yes
echo 2) No
set /p input=
if "!input!" == "1" goto saveend
if "!input!" == "2" goto home
:almostend2
cls
echo ????: Fools...
echo !elname!: Wha-
echo Patch: I am PATCH 1, "Platinum." I want to tell !name! something...
echo !name!: What?
echo Patch: I don't know if I should tell you, but
echo !kgname!: Go on.
echo Patch: I raised you. I found you in the middle of nowhere.
echo I turned you into a robot using advanced H2R (human to robot)
echo methods and made you Patch 10.
echo And you, !fgname!. You are Patch 9
echo ICTP (walking in): Yes, this is true.
echo Patch: What is this piece of junk?
echo ICTP: I was one of the smartest computers around for my time.
echo And who are you calling a piece of JUNK^^!
echo Patch: I'll just send my mini Patches to destroy all of you^^!
pause
cls
echo Patch@platinum-T5224~$ scripting-interface
echo : patchMini.initiate();
echo : patchMini.attack(patch-!name!, patch-!fgname!, 
echo human-!awname!, human-!frname!, incom, evil-!elname!, key-!kgname!);
pause
:fightpatchmini
cls
echo 27 Patch Minis attacked^^!
set enemyname=27 Patch Minis
set num=pm
set enpmpow=100
set enpmmagic=0
set enpmhp=10000
pause
goto fight
:killedpm
cls
echo You defeated all the Patch Minis^^!
pause
:almost2end2
cls
echo Patch: I still have the ultimate weapon,
echo the TIME WAND^^!^^!^^!
echo I can travel through time and space at will, and I will^^!
echo Patch@platinum-T5224~Gold  patchMini.forcestop();
echo Yes, I can see the future now. I can see, uh...
echo a child- not just a child, but a Levvi- with you, !name!.
echo But your friends aren't with you, your with
echo some other people. That's all I can see, for now...
:: The above represents some story in the next game, InCom 2: Working Title
pause
set room=end2
echo !elname!: You should fight me for practice...
echo Don't ask why...
pause
:attackevil2
cls
set hp=!hpmax!
echo !elname! attacked^^!
set enemyname=!elname!
set num=el2
set enel2pow=600
set enel2magic=400
set enel2hp=20000
pause
goto fight
:killedel2
cls
echo You defeated !elname!
echo You got 9001 exp
echo and Gold 400
pause >nul
set /a exp=!exp! + 9001
set /a money=!money! + 400
goto endpatch
:endpatch
cls
echo Patch: This is the LAST straw^^!
echo I guess I'm going to have ALL my Patches
echo to destroy you two.
pause
:patch2
cls
echo Patch 2, "Server," Attacked
pause >nul
set enemyname=Server
set num=p2
set enp2hp=1000
set enp2pow=400
set enp2magic=400
goto fight
:killedp2
cls
echo Hint: use potions to help you fight on^^!
echo Patch 3, "Begin," Attacked
pause >nul
set enemyname=Begin
set num=p3
set enp3hp=1354
set enp3pow=450
set enp3magic=451
goto fight
:killedp3
cls
echo Patch 4, "Easy," Attacked
pause >nul
set enemyname=Easy
set num=p4
set enp4hp=1680
set enp4pow=500
set enp4magic=510
goto fight
:killedp4
cls
echo Patch 5, "Clause," Attacked
pause >nul
set enemyname=Clause
set num=p5
set enp5hp=2000
set enp5pow=550
set enp5magic=550
goto fight
:killedp5
cls
echo Patch 6, "Wind," Attacked
pause >nul
set enemyname=Wind
set num=p6
set enp6hp=2400
set enp6pow=600
set enp6magic=603
goto fight
:killedp6
cls
echo Patch 7, "Lucky," Attacked
pause >nul
set enemyname=Lucky
set num=p7
set enp7hp=2700
set enp7pow=650
set enp7magic=660
goto fight
:killedp7
cls
echo Patch 8, "Memory," Attacked
pause >nul
set enemyname=Memory
set num=p8
set enp8hp=3000
set enp8pow=700
set enp8magic=700
goto fight
:killedp8
cls
echo You defeated all the Patches...
pause >nul
set hp=!hpmax!
echo Patch: Impressive, but I have something that will knock
echo you right out of your seat... It should be done any day
echo now...
echo But in the meantime
echo 	You gained a lot of magic power
if "!magicgain!" == "1" goto fightpatch
set /a magic=!magic!+240
set magicgain=1
:fightpatch
pause
cls
echo Patch attacked suddenly^^!
pause >nul
set enemyname=Patch
set num=ph
set enphhp=9000
set enphpow=1200000
set enphmagic=500
goto fight
:killedph
cls
echo Patch: It seems I h-have underestemated you...
echo You hurt me big t-t-time... but I won't die...
echo My pretty l-little slave that y-y-you
echo knew as 'family,' or 'bro,' once is know is now Patch 11, "Gamma."
echo !name!: Brother?
echo Patch: Yes, apperently.
echo H-he will soon evolve into Patch t-twelve, "Omega."
echo H-h-he should be c-coming any t-time soon...
pause
set hp=!hpmax!
cls
echo L-look, there he is. Here, have the Time Wand!
echo Gamma: ...
echo Patch: DON'T JUST STAND THERE, 
echo NOW TAKE THE WAND!
echo Gamma: ...
echo Patch: Also, your father is dead, !name!.
echo !name!: Wha...
echo !kgname!: Wow.
echo !fgname!: Really? His father is dead?
echo Patch: Yes, I trapped him and then he died of starvation
echo !name!: *sob* I don't even remember him, but...
pause
cls
echo ICTP: W-what's g-gZZoZZmeZZZZZZ... 
echo I, INCOM, AM AT YOUR COMMAND, LORD PATCH
echo !name!: Oh no... Wait a sec...
echo 	Suddenly, you saw a vision
cls
color F0
ping localhost -n 4 >nul
echo Xotsol: I will name you: InCom, for Intellegent Computer
echo I will use Vot's Lullaby to turn you on and off
echo Vot's Lullaby goes...
echo 	You heard Vot's Lullaby.
pause
color 0A
:fightincom
cls
set hp=!hpmax!
echo InCom attacked
pause >nul
set enemyname=InCom(ICTP)
set num=ic
set enichp=15000
set enicpow=706
set enicmagic=90000
goto fight
:killedic
cls
echo !name!'s HP: !hp!
echo.
echo 1) Sing
echo.
echo !enemyname!'s HP: 1
set /p input=
if "!input!" == "1" goto incomsing
:incomsing
cls
echo You sang Vot's Lullaby...
pause >nul
cls
echo InCom: INCOM: CODENAME ICTP, ERasing... memor-...ZZZZ
echo InCom: Hello, Xotsol... Huh?
echo REFRESHING MEMORY FROM TEMP MEMORY
echo InCom: !name!? Yes, it is me.
echo Patch: Yes, but in the near future, you're going to 
echo be separated again. But !name! is with !fgname!, a ghost
echo a little kid, and a FAT robot.
echo Don't know how though...
echo Whatever, you win. 11, come.
echo I bet you don't even know that you're travelling with
echo one of the si... Never mind.
echo How did you know Vot's Lullaby? That's my
echo only REAL weakness, besides Echo, *shrudder*
echo Here, you can have it.
echo InCom: Really?
echo Patch: No. Good-bye now.
echo !fgname!: One of the... six?
pause
cls
echo To be continued, this time, in InCom2
pause
cls
start thank.vbs
set room=0
set final=1
pause
cls
echo Would you like to save?
echo.
echo 1) Yes
echo 2) No
set /p input=
if "!input!" == "1" goto save
if "!input!" == "2" goto end
:chaosend
cls
if "!final!" == "1" (
	echo You traveled to the Chaos War Ending in 4361 AD
	pause >nul
	goto chaosend1
)
echo You do not need to be here
pause >nul
goto timemachine
:chaosend1
cls
echo !name!: Huh? Aren't you one of the Patches?
echo Easy: Yes, I am Patch 4, "Easy".
echo !elname!: Is that ME? I looked even better in the past!
echo !awname!: 
pause
goto home
:revive
cls
echo You do not need to be here
pause >nul
goto timemachine
:evilrevive
cls
echo You do not need to be here
pause >nul
goto timemachine

:talk4
cls
echo !fgname!: Why doesn't it work?
echo ????: I can answer that.
echo ????: You need a key^^!
pause
cls
echo NAME ENTRY
echo A guy who has keys to every teleporter, even time machines.
set /p kgname=
cls
echo !kgname!: The name's !kgname!.
echo !name!: I know you, you're the guy on that TV show who
echo steals teleporters with your key.
echo !kgname!: That's me, alright^^!
echo Here, let me help.
echo !kgname! unlocks the time machine.
echo !kgname!: Look out!
pause
set talk4=1
goto fightdragan
:fightdragan
cls
echo A corrupted dragan appeared!
pause
set enemyname=Corrupted Dragan
set num=dn
set endnpow=170
set endnmagic=189
set endnhp=930
goto fight
:killeddn
cls
echo You befriended the dragan
echo.
echo You got Gold 200
echo and 450 Experience
pause >nul
set /a money=!money! + 200
set /a exp=!exp! + 450
goto namedragan
:namedragan
cls
echo NAME ENTRY
echo Your new dragan
set /p dragan=
cls
echo !kgname!: What are you gonna call it?
echo !name!: I don't know, maybe !dragan!
pause
goto outerkingdom31
:outerkingdom31
cls
set /a room=31
echo What to do?
echo.
echo 1) Go on
echo 2) Go back
echo 3) Teleport home
set /p input=
if "!input!" == "1" goto timemachine
if "!input!" == "2" goto draganvillage
if "!input!" == "3" goto home

::##END##################################################################################################################
:end
cls
echo ==Cast===============================================
echo.
echo !name! - An energetic cyborg and Patch 10 ~ Beta
pause >nul
echo !fgname! - A strange cyborg girl. She is Patch 9 ~ Alpha
pause >nul
echo InCom - A VERY intellegent computer made by Dr. Xotsol Platinum.
echo !name!'s friend. ~ ICTP
pause >nul
echo !elname! - Evil prince who turned out not to be bad after all. ~ Darkwind
pause >nul
echo Patch - Robot made by Dr. Platinum but then... ~ Patch 1: Platinum
pause >nul
echo !frname! - A friend of yours. Traitor. ~ Hard
pause >nul
echo !kgname! - A person with keys to every teleporter and time machine. ~ Key
pause >nul
cls
echo Patches:
echo.
echo Patch 1: "Platinum"
echo Patch 2: "Server
echo Patch 3: "Begin"
echo Patch 4: "Easy
echo Patch 5: "Clause"
echo Patch 6: "Wind"
echo Patch 7: "Lucky"
echo Patch 8: "Memory"
echo Patch 9: "!fgname!"
echo Patch 10: "!name!"
echo Patch 11: "Gamma"
pause >nul
cls
echo.
echo.
echo FIN.
ping localhost -n 9 >nul
pause >nul
goto menu

::##OTHER################################################################################################################

:save
cls
echo Saving...
(echo hp=%hp%)>> Saves\storymode\%save%.SAV
(echo hpmax=%hpmax%)>> Saves\storymode\%save%.SAV
(echo exp=%exp%)>> Saves\storymode\%save%.SAV
(echo exptill=%exptill%)>> Saves\storymode\%save%.SAV
(echo money=%money%)>> Saves\storymode\%save%.SAV
(echo lvl=%lvl%)>> Saves\storymode\%save%.SAV
(echo power=%power%)>> Saves\storymode\%save%.SAV
(echo powergain=%powergain%)>> Saves\storymode\%save%.SAV
(echo weaponpow=%weaponpow%)>> Saves\storymode\%save%.SAV
(echo weaponprice=%weaponprice%)>> Saves\storymode\%save%.SAV
(echo pots=%pots%)>> Saves\storymode\%save%.SAV
(echo armupgrade=%armupgrade%)>> Saves\storymode\%save%.SAV
(echo room=%room%)>> Saves\storymode\%save%.SAV
(echo magic=%magic%)>> Saves\storymode\%save%.SAV
(echo asword=%asword%)>> Saves\storymode\%save%.SAV
(echo keys=%keys%)>> Saves\storymode\%save%.SAV
(echo element=%element%)>> Saves\storymode\%save%.SAV
(echo jewels=%jewels%)>> Saves\storymode\%save%.SAV
(echo pot50s=%pot50s%)>> Saves\storymode\%save%.SAV
(echo elname=%elname%)>> Saves\storymode\%save%.SAV
(echo ecname=%ecname%)>> Saves\storymode\%save%.SAV
(echo frname=%frname%)>> Saves\storymode\%save%.SAV
(echo fgname=%fgname%)>> Saves\storymode\%save%.SAV
(echo kgname=%kgname%)>> Saves\storymode\%save%.SAV
(echo awname=%awname%)>> Saves\storymode\%save%.SAV
(echo dragan=%dragan%)>> Saves\storymode\%save%.SAV
(echo talk=%talk%)>> Saves\storymode\%save%.SAV
(echo talk2=%talk2%)>> Saves\storymode\%save%.SAV
(echo talk3=%talk3%)>> Saves\storymode\%save%.SAV
(echo talk4=%talk4%)>> Saves\storymode\%save%.SAV
(echo talk5=%talk5%)>> Saves\storymode\%save%.SAV
(echo talk6=%talk6%)>> Saves\storymode\%save%.SAV
(echo talk7=%talk7%)>> Saves\storymode\%save%.SAV
(echo talk8=%talk8%)>> Saves\storymode\%save%.SAV
(echo final=%final%)>> Saves\storymode\%save%.SAV
(echo smission1=%smission1%)>> Saves\storymode\%save%.SAV
(echo cantgetjewel=%cantgetjewel%)>> Saves\storymode\%save%.SAV
(echo finalmessage=%finalmessage%)>> Saves\storymode\%save%.SAV
(echo ictp=%ictp%)>> Saves\storymode\%save%.SAV
(echo alphahp=%alphahp%)>> Saves\storymode\%save%.SAV
(echo alpha=%alpha%)>> Saves\storymode\%save%.SAV
(echo magicgain=%magicgain%)>> Saves\storymode\%save%.SAV
(echo name=%name%)>> Saves\storymode\%save%.SAV
::Magic
(echo spellfire1=%spellfire1%)>> Saves\storymode\%save%.SAV
(echo spellfire2=%spellfire2%)>> Saves\storymode\%save%.SAV
(echo spellfire3=%spellfire3%)>> Saves\storymode\%save%.SAV
(echo spellfire4=%spellfire4%)>> Saves\storymode\%save%.SAV
(echo spellfire5=%spellfire5%)>> Saves\storymode\%save%.SAV
(echo spellwater1=%spellwater1%)>> Saves\storymode\%save%.SAV
(echo spellwater2=%spellwater2%)>> Saves\storymode\%save%.SAV
(echo spellwater3=%spellwater3%)>> Saves\storymode\%save%.SAV
(echo spellwater4=%spellwater4%)>> Saves\storymode\%save%.SAV
(echo spellwater5=%spellwater5%)>> Saves\storymode\%save%.SAV
(echo spellwaterblast1=%spellwaterblast1%)>> Saves\storymode\%save%.SAV
(echo spellwaterblast2=%spellwaterblast2%)>> Saves\storymode\%save%.SAV
(echo spellwaterblast3=%spellwaterblast3%)>> Saves\storymode\%save%.SAV
(echo spellleafcutter1=%spellleafcutter1%)>> Saves\storymode\%save%.SAV
(echo spellleafcutter2=%spellleafcutter2%)>> Saves\storymode\%save%.SAV
(echo spellleafcutter3=%spellleafcutter3%)>> Saves\storymode\%save%.SAV
(echo spellleafcutter4=%spellleafcutter4%)>> Saves\storymode\%save%.SAV
(echo spellthunder1=%spellthunder1%)>> Saves\storymode\%save%.SAV
(echo spellthunder2=%spellthunder2%)>> Saves\storymode\%save%.SAV
(echo spellthunder3=%spellthunder3%)>> Saves\storymode\%save%.SAV
(echo spellult1=%spellult1%)>> Saves\storymode\%save%.SAV
(echo spellult2=%spellult2%)>> Saves\storymode\%save%.SAV
(echo spellult3=%spellult3%)>> Saves\storymode\%save%.SAV
(echo spellcometcrash=%spellcometcrash%)>> Saves\storymode\%save%.SAV
ping localhost 4 >nul
echo Success!
pause
if "!room!" == "a" goto draganvillage
if "!final!" == "1" goto end
goto home
:stats
cls
echo %name% is Level %lvl%
echo %exp%/%exptill% Experience
echo %hp%/%hpmax% Health
echo %money% Money
echo %pots% Potions
echo Weapon: %weapon%
echo.
echo Your power is %power%
echo Your weapon power is %weaponpow%
echo Your magic power is %magic%
echo Your element is %element%
echo.
pause
goto home
:warp1
cls
echo WARP ROOM
echo.
echo 1: Time Machine
echo 0: Home
set /p letter=
if %letter% == 0 goto home
:event
cls
echo Events are special things where you can do more things.
echo Put the .bat file in the 'Events'
echo then type in your save name and the event name.
echo When typing the event name do not include the .bat extension
pause
cls
set /p name= Load Name: 
if not exist Saves\storymode\%name%.SAV goto option
if exist Saves\storymode\%name%.SAV set load=1
for /f %%a in (Saves\storymode\%name%.SAV) do set %%a
goto event2
:event2
cls
set /p event=File Name: 
if not exist Events\%event%.bat goto event3
call Events\%event%.bat
:event3
cls
echo Doesn't exist!
pause >nul
goto option
:event4
cls
echo Events are special things where you can do more things.
echo Put the .bat file in the 'Events'
echo then type in your save name and the event name.
echo When typing the event name do not include the .bat extension
pause
goto event2
:event5
cls
set /p event=File Name: 
if not exist Events\%event%.bat goto event3
call Events\%event%.bat
:event6
cls
echo Doesn't exist!
pause >nul
goto ingameoption
:untalk1
cls
echo error0
echo Xotsol: Well, !name!, it was nice meating you.
echo I hope you can fix Patch to be his normal self
echo Vot: Are you leaving?
echo !awname!: Yes, I'm afraid so, if we have to defeat Patch.
echo !name!: Easy, she helped us.
echo Xotsol: What?^^!?^^! But, Easy is the most troublesome of them all^^!
echo Well, you kids better get a move on with your journey.
echo Vot is turning nine soon, in about a month or so...
echo You should be done by then, we're holding a party.
pause
goto timemachine

:sixtalk1
cls
echo error0
echo 

:newbattlesystem
cls
echo Feel free to try a battle.
echo.
echo Choose an enemy 
echo.
echo 1) Test Enemy
echo 2) No Man
echo 3) ICTP
echo 5) !name!
echo 6) Echo (DO NOT PICK)
echo 7) Wall
echo 8) Quiz.bat
echo 9) History Book
echo 10) sword.txt
echo 11) InComAI
echo 12) SuperGuy
echo 13) Notepad++
echo 14) Calculator
::need something here
echo 15) Ghost
echo 16) Zombie
echo 17) Rock Golem
echo 18) Golden Golem
echo 19) Iron Golem
echo 20) PatchMINI1
echo 21) PatchMINI2
echo 22) PatchMINI3
set /p input=
if %input% == 1 goto encountertest
if %input% == 2 goto encountertest2
if %input% == 3 goto encounterictp
if "!input!" == "6" goto encounterecho
:encountertest
cls
echo Test Enemy appeared!
pause >nul
set enemyname=Test Enemy
set num=test
set entestpow=5
set entestmagic=5
set entesthp=12
goto fight
:encountertest2
cls
echo the No Man appeared!
pause >nul
set enemyname=No Man
set num=test2
set entest2pow=280
set entest2magic=300
set entest2hp=120
goto fight
:encounterictp
cls
echo ICTP appeared!
pause >nul
set enemyname=ICTP
set num=ictp
set enictppow=5
set enictpmagic=0
set enictphp=10
goto fight

:encounterecho
cls
echo Echo appeared!
pause >nul
set enemyname=Echo
set num=echo
set enechopow=178497570
set enechomagic=180275170
set enechohp=1000
echo 101error
pause >nul
echo 102error
echo 404error
echo 900error
pause >nul
goto fight

:killedecho
cls
echo error2
goto menu

:killedtest
cls
goto overworld1

::###OVERWORLD DEMO#######################################################################################################

:overworld1loadhome
cls
set equip=null
set action=
set room=1
set w=119
set a=97
set s=115
set d=100
set m=109
set x=33
set y=7
:overworld1load2
cls
set action=
set room=1
set w=119
set a=97
set s=115
set d=100
set m=109
set x=1
:overworld1
echo.
echo =--
echo.
echo.
echo.
echo.     !itm1!
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo HP: !hp!/!hpmax!
echo.!equip!
echo.!action!
echo WASD to move, M to exit
colous 15 0 %x%,%y% "X"
colous readkey
set key=%errorlevel%
colous 0 0 %x%,%y% " "
colous 0 0 1,1
if %key% equ %w% set /a y-=1
if %key% equ %a% set /a x-=1
if %key% equ %s% set /a y+=1
if %key% equ %d% set /a x+=1
if %key% equ %m% goto overworldmenu

if %y% geq 16 set /a y=15
if %y% leq 0 set /a y=1
if %x% leq 0 goto overworld2load
if %x% geq 39 set /a x=38
if %x% equ 6 (
	if %y% equ 6 (
		set itm1= 
		set equip=Test Sword
		set action=You Got Test Sword^^!
	)
)
set /a critical=%random% %% 25
if "!critical!" == "7" (
	ping localhost -n 2 >nul
	goto encountertest
)
goto overworld1
:overworld2load
cls
set action=
set room=2
set w=119
set a=97
set s=115
set d=100
set m=109
set x=38
:overworld2
echo.
echo                                    --=
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo.
echo HP: !hp!/!hpmax!
echo.!equip!
echo.!action!
echo WASD to move, M to exit
colous 15 0 %x%,%y% "X"
colous readkey
set key=%errorlevel%
colous 0 0 %x%,%y% " "
colous 0 0 1,1
if %key% equ %w% set /a y-=1
if %key% equ %a% set /a x-=1
if %key% equ %s% set /a y+=1
if %key% equ %d% set /a x+=1
if %key% equ %m% goto overworldmenu

if %y% geq 16 set /a y=15
if %y% leq 0 set /a y=1
if %x% leq 0 set /a x=1
if %x% geq 39 goto overworld1load2
set /a critical=%random% %% 25
if "!critical!" == "7" (
	ping localhost -n 2 >nul
	goto encountertest
)
goto overworld2
:overworldmenu
cls
echo INCOM OVERWORLD DEMO
echo COMING SOON
pause
cls
goto home
call :overworld!room!

::##BATTLE#######################################################################################################

:fight
cls
echo !enemyname!'s HP: !en%num%hp!
if "!alpha!" == "1" (
	echo !fgname!'s HP: !alphahp!
)
::if "!enid!" == "000" (
::	echo !enemyname!'s HP: ????
::)
::if "!enid!" neq "000"
echo.
echo 1: Attack
echo 2: Magic
echo 3: Items
::echo 4: Party Members
echo.
if "!alpha!" == "1" (
	echo !fgname!'s HP: !alphahp!
) else (
	echo %name%'s HP: %hp%
)
set /p input=
if "!input!" == "1" goto attack
if %input% == 2 goto magiclist
if %input% == 3 goto inventory
:attack
cls
set /a critical=%random% %% 9
if "!critical!" == "4" goto miss
set /a en%num%hp=!en%num%hp! - (%power% + %weaponpow%)
set /a damage=%power% + %weaponpow%
if "!critical!" == "1" goto critical
echo It did !damage! damage to the enemy^^!
pause
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%pow!)
set /a damage=!en%num%pow!
echo You took !damage! damage^^!
pause
if %hp% leq 0 goto deathchoice
goto fight
:critical
cls
echo Critical Hit^^!
set /a en%num%hp=!en%num%hp! - (!power! / 2)
set /a damage=!damage! + (!power! / 2)
echo It did !damage! damage to the enemy^^!
pause
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%pow!)
set /a damage=!en%num%pow!
echo You took !damage! damage^^!
pause
if %hp% leq 0 goto deathchoice
goto fight
:miss
cls
echo You missed^^!
pause
set /a hp=%hp% - (!en%num%pow!)
set /a damage=!en%num%pow!
echo You took !damage! damage^^!
pause
if %hp% leq 0 goto deathchoice
goto fight
:magiclist
cls
echo Magic
echo.
echo 1: Basic Offense
if "!spellfire1!" == "1" (echo 2: Fire I)
::echo 3) Water I
::echo 4) Water Cannon I
::echo 5) Leaf Razor I
::echo 6) Thunder I
echo 3: Ultimate I
::echo 8) Ultimate II
::echo 9) Ultimate III
echo 4: Comet Crash
echo 0) Back
set /p input=
if "!inupt!" == "1" goto magicuse
if "!input!" == "2" goto usefire1
if "!input!" == "3" goto useult1
goto fight
:nomagic
cls
echo You do not have that spell...
pause
goto magiclist
:magicuse
cls
echo You used basic offensive magic!
pause
set /a en%num%hp=!en%num%hp! - (%magic% - 25)
set /a damage=!en%num%hp! - (%magic% - 25)
echo It did !damage! damage to the enemy!
pause
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:usefire1
if not "!spellfire1!" == "1" goto nomagic
cls
set /a en%num%hp=!en%num%hp! - (%magic% - 15)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:usewater1
if not "!spellwater1!" == "1" goto nomagic
cls
set /a en%num%hp=!en%num%hp! - (%magic% - 15)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:usewaterblast1
if not "!spellwaterblast1!" == "1" goto nomagic
cls
set /a en%num%hp=!en%num%hp! - (%magic% - 10)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:useleafcutter1
if not "!spellleafcutter1!" == "1" goto nomagic
cls
set /a en%num%hp=!en%num%hp! - (%magic% - 10)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:usethunder1
if not "!spellthunder1!" == "1" goto nomagic
cls
set /a en%num%hp=!en%num%hp! - (%magic% - 6)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:useult1
if not "!spellult1!" == "1" goto nomagic
cls
set /a en%num%hp=!en%num%hp! - (%magic% + 30)
if !en%num%hp! leq 0 goto killed
set /a hp=%hp% - (!en%num%magic!)
if %hp% leq 0 goto deathchoice
goto fight
:drinkpot
cls
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto inventory
)
set /a hp=%hp% +20
set /a pots=%pots% -1
goto fight
:drinkpot2
if %pots% lss 1 (
    echo You don't have any potions...
	    pause >nul
		goto fight
)
set /a hp=%hpmax%
set /a pots=%pots% - 1
goto fight
:killed
call :killed%num%

:inventory
cls
echo Items
echo.
echo 1) Small-Potions: !pots!
echo 2) Medium-Potions: !pot50s!
echo 3) High-Potions: !pot100s!
echo 4) Mega-Potions: !pot200s!
echo 5) Max-Potions: !potmaxs!
echo 0) Back
set /p input=
if "!input!" == "1" goto drinkpot2
if "!input!" == "0" goto fight
goto fight

:deathchoice
if "!gamemode!" == "1" goto death
if "!gamemode!" == "2" goto deathfree

::##SPECIAL MISSIONS#############################################################################################################
:error4
cls
echo 4 ERROR
echo.
echo You have not unlocked this special mission yet, or it is not
echo present in the game yet
pause >nul
goto smissions

:smissions
cls
echo SPECIAL MISSIONS
echo.
echo 1) The Past
set /p input=
if "!input!" == "1" goto specialmission1
goto home

:specialmission1
cls
if "!smission1!" == "0" goto error4
pause
goto error4

::##PROJECT TIME CAPSULE##########################################################################################################

:::ptc
::cd past
::cls
::echo PROJECT TIME CAPSULE
::echo.
::echo Choose a version
::echo.
::echo 1) Beta 1.0
::echo 2) Beta 1.1
::echo 3) Beta 1.1.01 Bugfix
::echo 4) Beta 1.2
::echo 5) Beta 1.3
::echo b) Back
::set /p past=
::if "!past!" == "1" goto versionb10
::if "!past!" == "2" goto versionb11
::if "!past!" == "3" goto versionb1101
::if "!past!" == "4" goto versionb12
::if "!past!" == "5" goto versionb13
::if "!past!" == "b" goto home2

:::versionb10
::cls
::cd beta
::cd 10
::call InCom.bat

:::versionb11
::cls
::cd beta
::cd 11
::call InCom.bat

:::versionb1101
::cls
::cd beta
::cd 1101
::call InCom.bat

:::versionb12
::cls
::cd beta
::cd 12
::call InCom.bat

:::versionb13
::cls
::cd beta
::cd 13
::call InCom.bat

:callfunction
cls
echo Debug option to call any label(:yourlabelhere)
echo Please type in the EXACT name of the label to go
echo to it(DO NOT INCLUDE THE :)
set /p input=
call :!input!
