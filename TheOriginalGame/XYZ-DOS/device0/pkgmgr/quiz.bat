@echo off
setlocal enabledelayedexpansion
:title
cls
echo This is the official XYZ-DOS Port of the quiz game that came
echo with InCom. You will get points for each question right
echo I'll be your host, Echo(yes, my name is echo)
echo So, do you want to play or not, kid?
echo.
echo 1) Play the game
echo 2) Leave
echo 3) Do nothing
echo 4) NO
set /p input=
if "!input!" == "1" goto start
if "!input!" == "2" goto xyz
if "!input!" == "3" goto nothing1
if "!input!" == "4" goto no1
:nothing1
cls
echo Doing nothing will get you nowhere
echo Now, do you want to play?
echo.
echo 1) Yes
echo 2) NO
echo 3) Still do nothing
set /p input=
if "!input!" == "1" goto start
if "!input!" == "2" goto no1
if "!input!" == "3" goto nothing2
goto start
:no1
cls
echo Well, TOO BAD
echo YOU WILL PLAY NO MATTER WHAT
pause >nul
goto start
:nothing2
cls
echo ANSWER ME OR I'LL KI-
echo just play
pause >nul
goto start
:start
cls
echo Remember, you are my... uh, slave(maybe)
echo I'll set you free after you complete my questions
echo DON'T EVEN THINK ABOUT RUNNING AWAY.
echo Now, where were we? Oh, yea
pause
goto level1
:level1
cls
echo First Question
echo This'll be an easy one
echo WHAT IS THE TITLE OF THE WINDOW
echo.
echo A) Batch RPG
echo B) InCom
echo C) XYZ-DOS
echo D) GET ME OUT OF HERE
set /p input=
if "%input%" == "a" goto wrong
if "%input%" == "b" goto wrong
if "%input%" == "c" goto right1
if "%input%" == "d" goto what1
:wrong
cls
echo Wrong. Try again
pause >nul
goto level1
:what1
cls
echo WHAT ARE YOU THINKING, TRYING TO RUN AWAY?
echo HAHAHAHAHA
pause >nul
goto level1
:right1
cls
echo You are correct. I don't believe it.
echo I thought you were super dumb
echo.
echo Earned 5 Z-Points
set /a points=!points! + 5
pause >nul
goto level2
:level2
cls
echo Second Question
echo WHAT AM I?
echo.
echo A) Echo
echo B) InCom Graphical UI
echo C) Awesome
echo D) Batch Programing
set /p input=
if "%input%" == "a" goto wrong
if "%input%" == "b" goto wrong
if "%input%" == "c" goto right2
if "%input%" == "d" goto right2
goto wrong
:right2
cls
echo You are correct. The next one wont be as easy. At all
echo.
echo Earned 7 Z-Points
set /a points=!points! + 7
pause >nul
goto level3
:level3
cls
echo Third Question
echo Which came first?
echo.
echo A) what
echo B) Chicken
echo C) Egg
echo D) NO
set /p input=
if "%input%" == "a" goto right3
if "%input%" == "b" goto wrong
if "%input%" == "c" goto wrong
if "%input%" == "d" goto wrong
goto wrong
:right3
cls
echo Correct...?
pause >nul
goto level4
:level4
cls
echo THIS IS NOT MULTIPLE CHOICE.
echo.
echo Forth Question
echo Name the linux distribution I'n thinking of.
echo IMPORTANT_no capitals please_IMPORTANT
set /p linux=
if "%linux%" == "manjaro linux" goto right4
goto wrong
:right4
echo Correct. I don't believe it, you win
echo Here's your prize, 20 Z-Points
echo.
echo Earned 20 Z-Points
set /a points=!points! + 20

:xyz
echo.
(echo points=%points%)>> saves\save.SAV
cd..