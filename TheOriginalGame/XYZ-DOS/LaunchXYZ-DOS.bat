@echo off
setlocal enabledelayedexpansion
TITLE XYZ-DOS
set points=0
set edit=0
set name1=0
set contact=0
set color=07
set name=
set openxyz=0
set call1=0
set call2=0
set update1=0
set username=Example
set password=ABC
set login=0
set hunter=0
set ypoints=0
cd device0
if exist usr\saves\save.SAV set load=1
for /f %%a in (usr\saves\save.SAV) do set %%a
if "!openxyz!" == "1" goto openxyzboot
echo Loading the X Kernel...
ping localhost -n 3 >nul
color !color!
echo Loading the core API...
ping localhost -n 2 >nul
echo Loading the package info...
ping localhost -n 2 >nul
echo Loading XYZ-DOS...
ping localhost -n 4 >nul
cls
:xyzshintro
echo Welcome to XYZ-DOS
echo.
echo Enter 'help' for more info.
echo.
:xyzsh
set /p shinput=!name!~$ 
if "!shinput!" == "help" goto help
if "!shinput!" == "clear" goto clear
if "!shinput!" == "halt" goto halt
if "!shinput!" == "packages" goto packagelist
if "!shinput!" == "pkgmgr" goto pkgmgr
if "!shinput!" == "save" goto save
if "!shinput!" == "call" goto call
if "!adventure!" == "1" (
	if "!shinput!" == "adventure" goto adventure
)
if "!quiz!" == "1" (
	if "!shinput!" == "quiz" goto quiz
)
if "!text!" == "1" (
	if "!shinput!" == "xedit" goto text
)
if "!contact!" == "1" (
	if "!shinput!" == "xcontact" goto contact
)
if "!name1!" == "1" (
	if "!shinput!" == "name" goto name
)
if "!color!" == "1" (
	if "!shinput!" == "xcolor" goto color
)
if "!yglobe!" == "1" (
	if "!shinput!" == "xglobe" goto yglobe
)
if "!hunter!" == "1" (
	if "!shinput!" == "hunter" goto hunter
)
echo Unknown command "!shinput!"
goto xyzsh

:help
cls
echo This is help
echo Type 'clear' to remove everything on the screen
echo Type 'halt' to save and turn the computer off
echo Type 'packages' to list all packages on the computer
echo Type 'pkgmgr' to launch the package manager
echo Type 'save' to save your progress
echo.
echo There are no programs, they are called packages.
echo.
echo To launch a package, just type in the name of it.
echo.
echo Some packages require some X-Points. They are earned by doing certain
echo tasks in a package.
pause
goto xyzsh

:clear
cls
goto xyzsh

:packagelist
echo Installed packages:
echo xyzsh - The XYZ Shell
echo xyzdrivers - Drivers
echo xkernel - The X Kernel
echo coreapi - Core API
echo pkginfo - Package Information
echo packages - List of Packages
echo pkgmgr - Package Manager
echo save - Save the game
if "!quiz!" == "1" echo quiz - A quiz game
if "!adventure!" == "1" echo adventure - A simple adventure game
if "!edit!" == "1" echo xedit - A simple text editor
goto xyzsh

:pkgmgr
cls
echo X-Points: !points!
echo.
echo Available Packages:
echo quiz - A Simple but Hard Quiz Game - Free
echo adventure - A simple adventure game - Free
echo xedit - A simple text editor. - 5 Z-Points
if "!text!" == "1" (
	echo xcontact - Contact another user, useless without xkernel2 - 10 Z-Points
	echo name - Give yourself a name - 7 Z-Points
)
if "!name1!" == "1" (
	echo xkernel2 - Version 2 of the X Kernel. Supports color and XNet - 50 Z-Points
)
if "!xkernel2!" == "1" (
	echo xcolor - Change color of background and text - 12 Z-Points
	echo xstorage - Allows for mounting extra storage devices - 20 Z-Points
	echo xglobe - YGlobe Simple Internet Browser - 30 Z-Points
)
if "!update1!" == "1" (
	echo update1 - The XYZ Update package. - 120 Z-Points
)
echo.
echo Choose a package by typing in the name of it. Type 'quit' to exit
set /p pkgchoice=
if "!pkgchoice!" == "adventure" (
	if "!adventure!" == "1" goto nobuy
	echo adventure - A simple adventure game >> packages\pkglist
	set adventure=1
	echo Bought^^!
	pause
	goto xyzsh
)
if "!pkgchoice!" == "quiz" (
	if "!quiz!" == "1" goto nobuy
	echo quiz - A simple quiz game >> packages\pkglist
	set quiz=1
	echo Bought^^!
	pause
	goto xyzsh
)
if "!pkgchoice!" == "xedit" (
	if "!text!" == "1" goto nobuy
	set text=1
	set /a points=!points! - 5
	echo Bought^^!
	pause
	goto xyzsh
)
if "!pkgchoice!" == "name" (
	if "!name1!" == "1" goto nobuy
	if not "!text!" == "1" goto notexist
	echo name - Change your name >> packages\pkglist
	set name1=1
	set /a points=!points! - 7
	echo Bought^^!
	pause
	goto xyzsh
)
if "!pkgchoice!" == "xkernel2" (
	if not "!name1!" == "1" goto notexist
	if "!xkernel2!" == "1" goto nobuy
	echo xkernel2 - Version 2 of the X Kernel >> packages\pkglist
	set xkernel2=1
	set /a points=!points! - 50
	echo Bought^^!
	pause
	goto xyzsh
)
if "!pkgchoice!" == "xcolor" (
	if not "!xkernel2!" == "1" goto notexist
	if "!color!" == "1" goto nobuy
	set color=1
	set /a points=!points! - 12
	echo Bought^^!
	pause
	goto xyzsh
)
if "!pkgchoice!" == "xcontact" (
	if not "!text!" == "1" goto notexist
	if "!contact!" == "1" goto nobuy
	set contact=1
	set /a points=!points! - 10
	echo Bought^^!
	pause
	goto xyzsh
)
if "!pkgchoice!" == "xglobe" (
	if not "!xkernel2!" == "1" goto notexist
	if "!yglobe!" == "1" goto nobuy
	set xglobe=1
	set /a points=!points! - 10
	echo Bought^^!
	pause
	goto xyzsh
)
if "!pkgchoice!" == "update1" (
	if not "!update1!" == "1" goto notexist
	if "!update!" == "1" goto nobuy
	cls
	echo By installing the XYZ-DOS Update package, you agree
	echo to the package overwriting and update existing packages
	echo.
	echo The XYZ-DOS Update package will update these 
	echo packages:
	echo.
	echo xcontact - 1.1
	echo xkernel2 - 2.10.0
	echo xglobe - 1.0a
	echo.
	pause
	set update=1
	set /a points=!points! - 120
	echo Bought^^!
	pause
	goto xyzsh
)
if "!pkgchoice!" == "quit" goto xyzsh
:notexist
echo This package does not exist^^!
goto xyzsh
:nobuy
echo You already have this package^^!
goto xyzsh
:notenough
echo You don't have enough Z-Points^^!
goto xyzsh




	
	
	
	
	
	


:save
echo Saving...
(echo points=%points%)>> saves\save.SAV
(echo adventure=%adventure%)>> saves\save.SAV
(echo quiz=%quiz%)>> saves\save.SAV
(echo text=%text%)>> saves\save.SAV
(echo color=%color%)>> saves\save.SAV
(echo text=%text%)>> saves\save.SAV
(echo contact=%contact%)>> saves\save.SAV
(echo call1=%call1%)>> saves\save.SAV
(echo call2=%call2%)>> saves\save.SAV
(echo name1=%name1%)>> saves\save.SAV
(echo xkernel2=%xkernel2%)>> saves\save.SAV
(echo yglobe=%yglobe%)>> saves\save.SAV
(echo update=%update%)>> saves\save.SAV
(echo username=%username%)>> saves\save.SAV
(echo password=%password%)>> saves\save.SAV
(echo login=%login%)>> saves\save.SAV
(echo hunter=%hunter%)>> saves\save.SAV
ping localhost -n 4 >nul
echo Success!
goto xyzsh
:halt
(echo points=%points%)>> saves\save.SAV
(echo adventure=%adventure%)>> saves\save.SAV
(echo quiz=%quiz%)>> saves\save.SAV
(echo text=%text%)>> saves\save.SAV
(echo color=%color%)>> saves\save.SAV
(echo name=%name%)>> saves\save.SAV
(echo contact=%contact%)>> saves\save.SAV
(echo call1=%call1%)>> saves\save.SAV
(echo call2=%call2%)>> saves\save.SAV
(echo name1=%name1%)>> saves\save.SAV
(echo xkernel2=%xkernel2%)>> saves\save.SAV
(echo yglobe=%yglobe%)>> saves\save.SAV
(echo update=%update%)>> saves\save.SAV
(echo username=%username%)>> saves\save.SAV
(echo password=%password%)>> saves\save.SAV
(echo login=%login%)>> saves\save.SAV
(echo hunter=%hunter%)>> saves\save.SAV
exit


:adventure
cls
echo You're going to fight the dragon, who set fire to the village and
echo the kingdom.
echo You are standing at the border of the village, with the
echo castle 2 leagues away.
echo You are a tall, skinny knight who has come to
echo the village to help the people.
echo.
echo What will you do?
echo.
echo 1) Go to the kingdom
echo 2) Go to the mysterious forest
set /p input=
if "!input!" == "1" goto kingdompath
if "!input!" == "2" goto mforest
:kingdompath
echo Earned 5 Z-Points
set /a points=!points! + 5
echo.
echo A bear blocks your path.
echo.
echo What will you do?
echo.
echo 1) Fight the bear recklessly
echo 2) RUN
echo 3) Trick the bear into falling into a nearby hole
set /p input=
if "!input!" == "1" goto death1
if "!input!" == "2" goto offkingdompath
if "!input!" == "3" goto kingdompath2
:offkingdompath
cls
echo Thanks for playing the adventure package alpha, part of XYZ-DOS Alpha 1
echo.
echo This part is not complete.
echo Your unsaved progress will still exist, but don't forget to save.
pause
goto bye
:kingdompath2
cls
echo Got 500 Z-Points, as an alpha testing gift
echo Come back any time
set /a points=!points! + 500
echo Thanks for playing the adventure package alpha, part of XYZ-DOS Alpha 1
echo.
echo This part is not complete.
echo Your unsaved progress will still exist, but don't forget to save.
pause
goto bye



:mforest
cls
echo Thanks for playing the adventure package alpha, part of XYZ-DOS Alpha 1
echo.
echo This part is not complete.
echo Your unsaved progress will still exist, but don't forget to save.
pause
goto bye





:death1
echo The bear strikes you then eats you.
echo.
echo YOU LOSE
echo Your unsaved progress will still exist, but don't forget to save.
pause

:bye
goto xyzsh


:quiz
cls
echo This is the official XYZ-DOS Port of the quiz game that came
echo with InCom. You will get points for each question right
echo I'll be your host, Echo(yes, my name is echo)
echo So, do you want to play or not, kid?
echo.
echo 1) Play the game
echo 2) Leave
echo 3) Do nothing
echo 4) NO
set /p input=
if "!input!" == "1" goto start
if "!input!" == "2" goto xyz
if "!input!" == "3" goto nothing1
if "!input!" == "4" goto no1
:nothing1
cls
echo Doing nothing will get you nowhere
echo Now, do you want to play?
echo.
echo 1) Yes
echo 2) NO
echo 3) Still do nothing
set /p input=
if "!input!" == "1" goto start
if "!input!" == "2" goto no1
if "!input!" == "3" goto nothing2
goto start
:no1
cls
echo Well, TOO BAD
echo YOU WILL PLAY NO MATTER WHAT
pause >nul
goto start
:nothing2
cls
echo ANSWER ME OR I'LL KI-
echo just play
pause >nul
goto start
:start
cls
echo Remember, you are my... uh, slave(maybe)
echo I'll set you free after you complete my questions
echo DON'T EVEN THINK ABOUT RUNNING AWAY.
echo Now, where were we? Oh, yea
pause
goto level1
:level1
cls
echo First Question
echo This'll be an easy one
echo WHAT IS THE TITLE OF THE WINDOW
echo.
echo A) Batch RPG
echo B) InCom
echo C) XYZ-DOS
echo D) GET ME OUT OF HERE
set /p input=
if "%input%" == "a" goto wrong
if "%input%" == "b" goto wrong
if "%input%" == "c" goto right1
if "%input%" == "d" goto what1
:wrong
cls
echo Wrong. Try again
pause >nul
goto level1
:what1
cls
echo WHAT ARE YOU THINKING, TRYING TO RUN AWAY?
echo HAHAHAHAHA
pause >nul
goto level1
:right1
cls
echo You are correct. I don't believe it.
echo I thought you were super dumb
echo.
echo Earned 5 Z-Points
set /a points=!points! + 5
pause >nul
goto level2
:level2
cls
echo Second Question
echo WHAT AM I?
echo.
echo A) Echo
echo B) InCom Graphical UI
echo C) Awesome
echo D) Batch Programing
set /p input=
if "%input%" == "a" goto wrong
if "%input%" == "b" goto wrong
if "%input%" == "c" goto right2
if "%input%" == "d" goto right2
goto wrong
:right2
cls
echo You are correct. The next one wont be as easy. At all
echo.
echo Earned 7 Z-Points
set /a points=!points! + 7
pause >nul
goto level3
:level3
cls
echo Third Question
echo Which came first?
echo.
echo A) The Wizardmaster
echo B) TheMasterX
echo C) ME
echo D) ICTP
set /p input=
if "%input%" == "a" goto right3
if "%input%" == "b" goto wrong
if "%input%" == "c" goto wrong
if "%input%" == "d" goto wrong
goto wrong
:right3
cls
echo Correct...?
pause >nul
goto level4
:level4
cls
echo THIS IS NOT MULTIPLE CHOICE.
echo.
echo Forth Question
echo Name the linux distribution I'n thinking of.
echo IMPORTANT_no capitals please_IMPORTANT
set /p linux=
if "%linux%" == "manjaro linux" goto right4
goto wrong
:right4
echo Correct. I don't believe it, you win
echo Here's your prize, 20 Z-Points
echo.
echo Earned 20 Z-Points
set /a points=!points! + 20
goto xyzsh

:color
cls
echo Pick your color
echo 0 - Black		8 - Gray
echo 1 - Blue		9 - Light Blue
echo 2 - Green		A - Light Green
echo 3 - Aqua		B - Light Aqua
echo 4 - Red		C - Light Red
echo 5 - Purple		D - Light Purple
echo 6 - Yellow		E - Light Yellow
echo 7 - White		F - Bright White
echo.
echo Write background color then text color
echo e.g 08 will give you gray text on black
set /p color=
color !color!
goto xyzsh



:contact
cls
if not "!xkernel2!" == "1" (
	echo You need X Kernel 2 or later
	goto xyzsh
)
echo ---xCONTACT------------------------------------
echo.
echo.
echo Contacts:
echo.
echo 1) TheMasterX
echo 2) unknownhacker
echo 3) na7547
echo A) Type in an user's name (Not Working)
echo.
echo 0) Exit
echo.
set /p input=
if "!input!" == "1" goto contactx1
if "!call1!" == "1" (
	if "!input!" == "2" goto contacthacker1
)
if "!call2!" == "1" (
	if "!input!" == "3" goto contactna1
)
if "!input!" == "A" (
	echo error4
	goto xyzsh
)
goto xyzsh
:contactx1
cls
echo Calling User "TheMasterX"...
ping localhost -n 12 >nul
echo Connected.
ping localhost -n 5 >nul
echo TheMasterX: So I see that the contact package is working
ping localhost -n 6 >nul
echo TheMasterX: I've bean working on this speech recognition package
ping localhost -n 5 >nul
echo TheMasterX: I'm using it right night. Its kidney bug eat.
ping localhost -n 6 >nul
echo TheMasterX: I've also been working on a weak err... internet called Y-Net
ping localhost -n 6 >nul
echo TheMasterX: A beta version is included with X Kernel 2
ping localhost -n 9 >nul
echo TheMasterX: It is fine with contact, but I tried some other packages,
echo and there were bugs and errors.
ping localhost -n 5 >nul
echo TheMasterX: I also heard about some guy making a clone of XYZ-DOS
ping localhost -n 6 >nul
echo User "unknownhacker" Connecting...
ping localhost -n 4 >nul
echo TheMasterX: Who's this guy?
ping localhost -n 8 >nul
echo User "unknownhacker" Connected.
ping localhost -n 4 >nul
echo unknownhacker: That would be me, unknownhacker.
ping localhost -n 6 >nul
echo TheMasterX: Your one of the three beta testers, aren't you?
ping localhost -n 8 >nul
echo unknownhacker: Yes, I am. I came to wonder, when is the file manager
echo coming out?
ping localhost -n 9 >nul
echo TheMasterX: I think it should be out when you get the xstorage
echo package is out.
ping localhost -n 5 >nul
echo TheMasterX: I have to go you two. Bye.
ping localhost -n 3 >nul
echo User "TheMasterX" Disconnected
ping localhost -n 4 >nul
echo unknownhacker: I have to go too. Bye.
ping localhost -n 3 >nul
echo User "unknownhacker" Disconnected
echo Ending Call.
set call1=1
goto xyzsh
:contactx2
cls
echo Calling User "TheMasterX"...
ping localhost -n 12 >nul
echo Connected.
ping localhost -n 6 >nul
echo TheMasterX: Hey again.
ping localhost -n 6 >nul
echo TheMasterX: I've found the third beta tester.
ping localhost -n 6 >nul
echo TheMasterX: She's a time traveler from 47XX A.D.
ping localhost -n 7 >nul
echo TheMasterX: Her username is na7547
ping localhost -n 6 >nul
echo TheMasterX: I don't know what that means.
ping localhost -n 6 >nul
echo TheMasterX: I should call unknownhacker just to give him the news
ping localhost -n 4 >nul
echo User "TheMasterX" calling user "unknownhacker"...
ping localhost -n 11 >nul
echo User "unknownhacker" connected
ping localhost -n 5 >nul
echo unknownhacker: What's going on?
ping localhost -n 6 >nul
echo TheMasterX: I think I've found the third beta tester.
ping localhost -n 7 >nul
echo unknownhacker: What is his name?
ping localhost -n 6 >nul
echo TheMasterX: One, it's a girl. Two, her name is na7547
ping localhost -n 6 >nul
echo unknownhacker: I knew that.
ping localhost -n 6 >nul
echo TheMasterX: Do you know how to clear the screen?
ping localhost -n 3 >nul
echo unknownhacker: No.
ping localhost -n 6 >nul
echo TheMasterX: Just type /clear. Any command starts with / in contact.
ping localhost -n 6 >nul
echo unknownhacker: Oh, Okay.
ping localhost -n 4 >nul
clear
echo unknownhacker: I think I got that.
ping localhost -n 4 >nul
echo User "na7547" Connecting...
ping localhost -n 8 >nul
echo User "na7547" Connected.
ping localhost -n 6 >nul
echo na7547: Hey, I thought I would meet the other beta testers
ping localhost -n 6 >nul
echo TheMasterX: Well, there is unknownhacker and !name!
ping localhost -n 5 >nul
echo TheMasterX: !name! doesn't talk much. I think he has a broken copy of xcontact.
ping localhost -n 5 >nul
echo
:contacthacker1
cls
echo Coming Soon
goto xyzsh
:contactna1
cls
echo Coming Soon
goto xyzsh


:name
set /p name=Enter new name: 
goto xyzsh

:text
cls
cd usr\home\Documents
echo ---xEDIT------------------------------------
echo.
echo Choose an operation
echo.
echo 1) New file
echo 2) View file
echo 0) Exit
echo.
set /p input=? 
if "!input!" == "1" goto textnewfile
if "!input!" == "2" goto textopenfile
goto xyzsh
:textnewfile
cls
set /p textfile=What is the new file name? 
goto textedit
:textopenfile
dir /w
set /p input=File name? 
cls
type !input!
goto textexit
:textedit
cls
echo ---xEDIT------------------------------------
echo.
set /p text1=
:textendofile
echo ---END OF FILE--- C - Clear file  S - Save  E - Exit w/o saving
set /p input=? 
if "!input!" == "c" goto textedit
if "!input!" == "s" goto textsave
if "!input!" == "e" goto textexit
goto textendofile
:textsave
if exist !textfile! del !textfile!
echo Save name is !textfile!
(echo %text1%)>> !textfile!
echo Saving complete.
:textexit
cd..
cd..
goto xyzsh

:yglobe
cls
echo ---xGLOBE---------------------------------------------------
echo.
echo Choose an operation
echo.
echo 1) Reconmended Websites
echo 2) TheMasterX's Website
echo 3) URL
echo 0) Exit
echo.
set /p input=? 
if "!input!" == "1" goto yglobewebsites
if "!input!" == "2" goto yglobemasterweb
if "!input!" == "3" goto yglobeurl
goto xyzsh
:yglobewebsites
cls
echo ---xGLOBE---------------------------------------------------
echo.
echo 1) SkyCommunity
echo 2) XYZ-DOS Forums
echo 3) YDownloadHost
echo 0) Back
set /p input=? 
if "!input!" == "1" goto yglobeskycom
if "!input!" == "2" goto yglobe404
if "!input!" == "3" goto yglobe404
goto xyzsh
:yglobeurl
cls
echo Under Construction
goto xyzsh
:yglobemasterweb
cls
echo ---xGLOBE-----TheMasterX's Website--------------------------
echo Enter 'quit' to exit YGlobe
echo.
echo        Welcome to TheMasterX's Website.
echo ------------------------------------------------
echo 1) Essays         2) XYZ-DOS           3) About
echo.
echo News
echo ------------------------------------------------
echo.
echo  Why proprietary software was the past, and now the future.
echo  ----------------------------------------------------------
echo  2/24/2042
echo.
echo  Proprietary software was tooken over by open source. I
echo  have reasons why closed source software should rule again.
echo 		4) READ MORE
echo.
echo.
echo 2042 - TheMasterX
echo.
set /p input=
if "!input!" == "1" goto yglobe404
if "!input!" == "2" goto yglobemasterxyzdos
if "!input!" == "3" goto yglobe404
if "!input!" == "4" goto yglobe404
if "!input!" == "quit" goto xyzsh
goto yglobemasterweb
:yglobe404
cls
echo ---xGLOBE---------------------------------------------------
echo Enter 'quit' to exit YGlobe
echo.
echo 404 Error...
echo This is mainly caused by incompletion of the game.
echo.
pause
goto yglobe
:yglobemasterxyzdos
cls
echo ---xGLOBE-----TheMasterX's Website--------------------------
echo Enter 'quit' to exit YGlobe
echo.
echo XYZ-DOS - Text-based is the future
echo ------------------------------------------------
echo.
echo   -News-----------------------------------------
echo   XYZ-DOS Upgrade package
echo 	2/27/2042
echo     The XYZ-DOS upgrade package should be here by
echo 	 the time you read this. 1) Find Out More
set update1=1
echo 0) Home
echo.
set /p input=
if "!input!" == "1" goto yglobe404
if "!input!" == "0" goto yglobemasterweb
if "!input!" == "quit" goto xyzsh
goto yglobemasterxyzdos

:yglobeskycom
cls
echo ---xGLOBE-----SkyCommunity----------------------------------
echo Enter 'quit' to exit YGlobe
echo.
echo   S K Y  C O M M U N I T Y    1) Log In   2) Sign Up
echo --------------------------------------------------------
if "!login!" == "1" (echo Logged in as !username!)
echo.
echo New Messages -
echo.
echo 	3-Now compatible with YGlobe.  NEW
echo 	4-OpenXYZ Getting closer. NEW
echo 	5-How to improve on posting messages
echo 	6-Working on compatibility
echo Sticky Messages -
echo 	7-Rules
echo 	8-XYZ-DOS Official Beta Download
echo.
echo 										2042 Skyy
echo.
set /p input=
if "!input!" == "1" goto yglobelogin
if "!input!" == "2" goto yglobesignup
if "!input!" == "3" goto yglobe404
if "!input!" == "4" goto yglobeopenxyzsoon
if "!input!" == "5" goto yglobe404
if "!input!" == "6" goto yglobe404
if "!input!" == "7" goto yglobe404
if "!input!" == "8" goto yglobexyzdownload
if "!input!" == "quit" goto xyzsh
:yglobexyzdownload
cls
echo ---xGLOBE-----SkyCommunity----------------------------------
echo Enter 'quit' to exit YGlobe
echo.
echo   S K Y  C O M M U N I T Y
echo --------------------------------------------------------
echo.
echo Post - XYZ-DOS Official Beta
echo ------------------------------------------------------------------------------
echo                         :
echo By: Skyy                :  I have gotten an ISO file of XYZ-DOS. The OS that
echo On 2/25/42              :  will bring back text-based OSes, and proprietory
echo Rank: Admin             :  software.
echo                         :
echo                         :  NOTE: Don't use the contact package.
echo                         :  1) Download here
echo                         :
echo ------------------------------------------------------------------------------
echo                         :
echo By: unknownhacker       :  This seems interesting that you got a copy without
echo On 2/25/42              :  TheMasterX knowing. I can't tell if this works
echo Rank: Low-Level Cloud   :  because I am one of the beta testers, and XYZ-DOS
echo                         :  doesn't have a partition manager or a way to write
echo                         :  to a usb.
echo                         :
echo ------------------------------------------------------------------------------
echo                         :
echo By: H a p p y F a c e   :  I'm going to try this.
echo On 2/26/42              :  EDIT 2/28/42: I tried this along with the tool
echo Rank: Top of the tree   :  that unknownhacker made. Why is he 'bringing back
echo                         :  text-based'? It's dumb!
echo                         :
echo ------------------------------------------------------------------------------
echo                         :
echo By: 
echo.
echo 0) Back                                                   2042 Skyy
set /p input=
if "!input!" == "1" goto yglobe404
if "!input!" == "0" goto yglobeskycom
if "!input!" == "quit" goto xyzsh
:yglobelogin
cls
echo ---YGLOBE-----SkyCommunity----------------------------------
echo Enter 'quit' to exit YGlobe
echo.
echo   S K Y  C O M M U N I T Y
echo --------------------------------------------------------
echo.
set /p input=Username?
if not "!input!" == "!username!" goto yglobeskyerror
set /p input=Password?
if not "!input!" == "!password!" goto yglobeskyerror
set login=1
goto yglobeskycom
:yglobesignup
cls
echo ---YGLOBE-----SkyCommunity----------------------------------
echo Enter 'quit' to exit YGlobe
echo.
echo   S K Y  C O M M U N I T Y
echo --------------------------------------------------------
set /p username=New Username? 
set /p password=New Password? 
goto yglobeskycom
:yglobeopenxyzsoon
cls
echo ---YGLOBE-----SkyCommunity----------------------------------
echo Enter 'quit' to exit YGlobe
echo.
echo   S K Y  C O M M U N I T Y
echo --------------------------------------------------------
echo.
echo Post - OpenXYZ Getting Closer
echo ------------------------------------------------------------------------------
echo                         :
echo By: unknownhacker       :  Hey guys. Work is almost done on OpenXYZ. It
echo On 2/28/42              :  is an open-source XYZ-DOS clone. As you may know,
echo Rank: Low-level Cloud   :  I am one of the XYZ-DOS beta testers. I am using 
echo                         :  my beta testing to research for OpenXYZ.
echo                         :	However I have released the unofficial package
echo                         :	installer. 1) Get it now
echo                         :
echo ------------------------------------------------------------------------------
echo                         :
echo By: Skyy                :  I can't wait until it's released. I've
echo On 2/28/42              :  thought ever since you announced you were working
echo Rank: Admin             :	on it that it was going to be interesting.
echo                         :
echo                         :
echo ------------------------------------------------------------------------------
echo                         :
echo By: H a p p y F a c e   :  This is great. Are the other 2 testers on here?
echo On 2/28/42              :
echo Rank: Top of the tree   :
echo                         :
echo.
echo 0) Back                                                   2042 Skyy
set /p input=
if "!input!" == "1" goto installhunter
if "!input!" == "0" goto yglobeskycom
if "!input!" == "quit" goto xyzsh


:installhunter
cls
echo This will install the 'hunter' package
echo on your computer. This package is not supported
echo by TheMasterX.
echo.
echo Do you still want to install?
echo 1) Yes
echo 2) No
set /p input=
if "!input!" == "1" (
	if "!hunter!" == "1" goto nobuy
	copy hunter/hunter packages
	echo hunter - An unofficial package manager >> packages\pkglist
	set hunter=1
	echo Installed
	pause
	goto xyzsh
)
if "!input!" == "2" goto xyzsh
goto xyzsh


:hunter
cls
echo Y-Points: !ypoints!
echo.
echo Available Packages:
echo openxyz-trailer - Trailer for the upcoming OS, OpenXYZ - Free
echo incom-demo - A demo of InCom, exclusive to XYZ-DOS and OpenXYZ - Free
echo yspy - Spy on a xcontact conversation - 10 Y-Points
set /p input=
if "!input!" == "openxyz-trailer" (
	if "!openxyztrailer!" == "1" goto nobuy
	echo openxyz-trailer - Trailer for the upcoming OS, OpenXYZ >> packages\pkglist
	set openxytrailer=1
	echo Bought
	echo Thanks for downloading the trailer. I have hope for this OS.
	echo Take 15 free Y-Points, used in hunter to get stuff because
	echo X-Points only work in pkgmgr. Y-Points works with most online
	echo sevices.
	set ypoints+=15
	pause
	goto xyzsh
)
if "!input!" == "incom-demo" (
	if "!incomdemo!" == "1" goto nobuy
	echo incom-demo - A demo of InCom >> packages\pkglist
	set incomdemo=1
	echo Bought
	pause
	goto xyzsh
)
if "!input!" == "yspy" (
	if "!yspy!" == "1" goto nobuy
	set yspy=1
	echo Bought
	pause
	goto xyzsh
)
goto xyzsh

:yspy
cls
echo.
set /p input=Spy on who? 
if "!input!" == "TheMasterX" (
	goto spymasterx
)
:spymasterx
cls
echo Activating proxy...
ping localhost -n 3 >nul
echo Listening...
ping localhost -n 3 >nul
echo Doing magical stuff...
ping localhost -n 7 >nul
cls
echo TheMasterX: But what if the child() process is broken?
ping localhost -n 5 >nul
echo 





:openxyz
TITLE OpenXYZ
cls













:call
echo Call function - Type in label to go there
set /p call=
call :!call!
:syserror
cls
echo System error. Press any key to recover.
pause >nul
goto xyzsh
:critsyserror
echo CRITICAL SYSTEM ERROR
echo PLEASE RESTART THE SYSTEM
pause >nul
goto critsyserror